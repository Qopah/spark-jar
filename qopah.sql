-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: qopah_core
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qp_agent`
--

DROP TABLE IF EXISTS `qp_agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_agent` (
  `agent_id` int(200) NOT NULL,
  `agent_number` int(50) NOT NULL,
  `phone_number` varchar(50) NOT NULL,
  `location` varchar(100) NOT NULL,
  `name_of_business` varchar(100) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`agent_number`),
  KEY `agent_id` (`agent_id`),
  CONSTRAINT `qp_agent_ibfk_2` FOREIGN KEY (`agent_id`) REFERENCES `qp_client` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_agent`
--

LOCK TABLES `qp_agent` WRITE;
/*!40000 ALTER TABLE `qp_agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_airtime`
--

DROP TABLE IF EXISTS `qp_airtime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_airtime` (
  `airtime_id` int(200) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL,
  `amount` double NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`airtime_id`),
  KEY `airtime_user_fk_idx` (`user_id`),
  CONSTRAINT `airtime_user_fk` FOREIGN KEY (`user_id`) REFERENCES `qp_client` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_airtime`
--

LOCK TABLES `qp_airtime` WRITE;
/*!40000 ALTER TABLE `qp_airtime` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_airtime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_api_user`
--

DROP TABLE IF EXISTS `qp_api_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_api_user` (
  `app_id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(45) NOT NULL,
  `secret_key` varchar(45) NOT NULL,
  `api_key` varchar(200) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `auth_key` varchar(200) DEFAULT '',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_api_user`
--

LOCK TABLES `qp_api_user` WRITE;
/*!40000 ALTER TABLE `qp_api_user` DISABLE KEYS */;
INSERT INTO `qp_api_user` VALUES (1,'SMS','sms@qopah.com','$2y$13$BiRWiF5acxIc715RcVt./.RqfhpiwpbymTiW4NbCRlEK4BEPTCRNO',1,'NWGIji1hGJlQ2hOHfxTBZssXrf1-rz31','2016-12-03 08:39:01','2016-11-30 09:34:46'),(2,'USSD','ussd@qopah.com','$2y$13$BiRWiF5acxIc715RcVt./.RqfhpiwpbymTiW4NbCRlEK4BEPTCRNO',1,'NWGIji1hGJlQ2hOHfxTBZssXrf1-rz31	','2016-12-03 08:41:22','2016-12-01 13:52:07'),(3,'Qopah Admin','admin@qopah.com','$2y$13$4t8.d057gILEnuMiqgsuIOIOcm3eSHIcBY/35BhdnrPQFHIDL3CGS',1,'RSTbHLetC3PmS_054KkPJz6o0W2vuJZG','2016-12-03 09:00:05','2016-12-03 09:00:05');
/*!40000 ALTER TABLE `qp_api_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_cash`
--

DROP TABLE IF EXISTS `qp_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_cash` (
  `cash_id` int(200) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `status` int(5) NOT NULL,
  `user_id` int(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cash_id`),
  KEY `user_id_fk_idx` (`user_id`),
  CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `qp_client` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_cash`
--

LOCK TABLES `qp_cash` WRITE;
/*!40000 ALTER TABLE `qp_cash` DISABLE KEYS */;
INSERT INTO `qp_cash` VALUES (1,350000,0,170,'2017-02-23 21:00:00','2017-02-23 21:00:00'),(2,175000,0,171,'2017-01-31 21:00:00','2017-01-31 21:00:00'),(3,100000,0,172,'2017-02-01 21:00:00','2017-02-01 21:00:00'),(4,300000,0,173,'2017-02-20 21:00:00','2017-02-20 21:00:00'),(5,200000,0,173,'2017-02-10 21:00:00','2017-02-10 21:00:00'),(6,1750000,0,174,'2017-01-31 21:00:00','2017-01-31 21:00:00'),(7,351000,0,175,'2017-01-31 21:00:00','2017-01-31 21:00:00'),(8,1000000,0,178,'2017-02-26 21:00:00','2017-02-26 21:00:00');
/*!40000 ALTER TABLE `qp_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_client`
--

DROP TABLE IF EXISTS `qp_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_client` (
  `user_id` int(200) NOT NULL AUTO_INCREMENT,
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `phone_no` varchar(100) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `pin` text NOT NULL,
  `language_id` int(5) NOT NULL DEFAULT '1',
  `email` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `meter_no` varchar(45) DEFAULT NULL,
  `account_status` int(3) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `phone_no_UNIQUE` (`phone_no`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `meter_no_UNIQUE` (`meter_no`)
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_client`
--

LOCK TABLES `qp_client` WRITE;
/*!40000 ALTER TABLE `qp_client` DISABLE KEYS */;
INSERT INTO `qp_client` VALUES (170,'Clive','Moruri','254720216241','2260070','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:45:19','2017-02-28 08:45:01'),(171,'Elijah ','Wachira','254711570991','10976088','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:46:24','2017-02-28 08:46:24'),(172,'Hilda','Wambui','254722760529','4442568','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:57:08','2017-02-28 08:48:03'),(173,'Lubsol','Kenya LTD.','254733610098','3000031161','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:49:42','2017-02-28 08:49:42'),(174,'Ssunsi','Merchants Ltd.','254722799179','13705987','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:54:15','2017-02-28 08:54:15'),(175,'White Ridge','Dairy Farm','254731621890','254721667360','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 08:56:37','2017-02-28 08:56:37'),(178,'Elijah ','Mose','2547115709911','109760881','Dr7Bn+4jjA9laP1Tf47cQw==',1,NULL,NULL,NULL,1,'2017-02-28 09:31:34','2017-02-28 09:31:34');
/*!40000 ALTER TABLE `qp_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_electricity`
--

DROP TABLE IF EXISTS `qp_electricity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_electricity` (
  `electricity_id` int(200) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL,
  `amount` double NOT NULL,
  `tokens` int(200) NOT NULL,
  `meter_no` varchar(200) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`electricity_id`),
  KEY `electricity_user_idx` (`user_id`),
  CONSTRAINT `electricity_user` FOREIGN KEY (`user_id`) REFERENCES `qp_client` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_electricity`
--

LOCK TABLES `qp_electricity` WRITE;
/*!40000 ALTER TABLE `qp_electricity` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_electricity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_float`
--

DROP TABLE IF EXISTS `qp_float`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_float` (
  `float_id` int(50) NOT NULL AUTO_INCREMENT,
  `agent_number` int(50) NOT NULL,
  `amount` double DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`float_id`),
  KEY `agent_number` (`agent_number`),
  CONSTRAINT `qp_float_ibfk_1` FOREIGN KEY (`agent_number`) REFERENCES `qp_agent` (`agent_number`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_float`
--

LOCK TABLES `qp_float` WRITE;
/*!40000 ALTER TABLE `qp_float` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_float` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_language`
--

DROP TABLE IF EXISTS `qp_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_language` (
  `language_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `alias` varchar(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_language`
--

LOCK TABLES `qp_language` WRITE;
/*!40000 ALTER TABLE `qp_language` DISABLE KEYS */;
INSERT INTO `qp_language` VALUES (1,'English','en','2016-11-07 10:39:46','2016-11-07 10:39:46'),(2,'Swahili','sw','2016-11-07 10:39:46','2016-11-07 10:39:46');
/*!40000 ALTER TABLE `qp_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_outbox`
--

DROP TABLE IF EXISTS `qp_outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_outbox` (
  `outbox_id` int(200) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL,
  `message` text NOT NULL,
  `message_id` text NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `phone_no` varchar(200) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`outbox_id`),
  KEY `outbox_user_fk_idx` (`user_id`),
  CONSTRAINT `outbox_user_fk` FOREIGN KEY (`user_id`) REFERENCES `qp_client` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_outbox`
--

LOCK TABLES `qp_outbox` WRITE;
/*!40000 ALTER TABLE `qp_outbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_outbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_sme`
--

DROP TABLE IF EXISTS `qp_sme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_sme` (
  `sme_id` int(100) NOT NULL AUTO_INCREMENT,
  `sme_name` varchar(40) NOT NULL,
  `post_office_box` varchar(40) NOT NULL,
  `physical_address` varchar(40) DEFAULT NULL,
  `certificate_of_registration` varchar(40) NOT NULL,
  `applicant` int(200) NOT NULL,
  `status` int(200) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sme_id`),
  UNIQUE KEY `certificate_of_registration` (`certificate_of_registration`),
  KEY `applicant` (`applicant`),
  CONSTRAINT `qp_sme_ibfk_3` FOREIGN KEY (`applicant`) REFERENCES `qp_client` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_sme`
--

LOCK TABLES `qp_sme` WRITE;
/*!40000 ALTER TABLE `qp_sme` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_sme` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_sme_cash`
--

DROP TABLE IF EXISTS `qp_sme_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_sme_cash` (
  `sme_cash_id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` double NOT NULL,
  `sme_id` int(100) NOT NULL,
  `currency` varchar(10) NOT NULL,
  `period` int(1) NOT NULL,
  `delivery_method` varchar(30) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sme_cash_id`),
  KEY `sme_id` (`sme_id`),
  CONSTRAINT `qp_sme_cash_ibfk_1` FOREIGN KEY (`sme_id`) REFERENCES `qp_sme` (`sme_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_sme_cash`
--

LOCK TABLES `qp_sme_cash` WRITE;
/*!40000 ALTER TABLE `qp_sme_cash` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_sme_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_sms_activity`
--

DROP TABLE IF EXISTS `qp_sms_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_sms_activity` (
  `sms_id` int(200) NOT NULL AUTO_INCREMENT,
  `from` varchar(15) DEFAULT NULL,
  `to` varchar(15) DEFAULT NULL,
  `text` text,
  `date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `link_id` int(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sms_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_sms_activity`
--

LOCK TABLES `qp_sms_activity` WRITE;
/*!40000 ALTER TABLE `qp_sms_activity` DISABLE KEYS */;
INSERT INTO `qp_sms_activity` VALUES (1,'254714293809','223344','testing ..',NULL,564632,'2017-03-09 13:33:38','2017-03-09 13:33:38');
/*!40000 ALTER TABLE `qp_sms_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_transaction`
--

DROP TABLE IF EXISTS `qp_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_transaction` (
  `transaction_id` int(200) NOT NULL AUTO_INCREMENT,
  `user_id` int(200) NOT NULL,
  `transaction_type` enum('cash','airtime','electricity','float','sme_cash') NOT NULL,
  `transaction_type_id` int(200) NOT NULL,
  `credit` double DEFAULT NULL,
  `debit` double DEFAULT NULL,
  `status` int(11) NOT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `date` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `app_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_user_idx` (`user_id`),
  KEY `transaction_app_id_idx` (`app_id`),
  CONSTRAINT `transaction_app_id` FOREIGN KEY (`app_id`) REFERENCES `qp_api_user` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_user` FOREIGN KEY (`user_id`) REFERENCES `qp_client` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_transaction`
--

LOCK TABLES `qp_transaction` WRITE;
/*!40000 ALTER TABLE `qp_transaction` DISABLE KEYS */;
INSERT INTO `qp_transaction` VALUES (1,170,'cash',1,0,350000,0,'254720216241','2017-02-24 00:00:00',2,'2017-02-23 21:00:00','2017-02-23 21:00:00'),(2,171,'cash',2,0,175000,0,'254711570991','2017-02-28 11:46:24',2,'2017-02-28 08:46:24','2017-02-28 08:46:24'),(3,172,'cash',3,0,102000,0,'254722760529','2017-02-02 00:00:00',2,'2017-02-01 21:00:00','2017-02-01 21:00:00'),(4,173,'cash',4,0,300000,0,'254733610098','2017-02-21 00:00:00',2,'2017-02-20 21:00:00','2017-02-20 21:00:00'),(5,173,'cash',5,0,210000,0,'254733610098','2017-02-11 00:00:00',2,'2017-02-10 21:00:00','2017-02-10 21:00:00'),(6,174,'cash',6,0,1785000,0,'254722799179','2017-02-01 00:00:00',2,'2017-01-31 21:00:00','2017-01-31 21:00:00'),(7,175,'cash',7,0,358020,0,'254731621890','2017-02-01 00:00:00',2,'2017-01-31 21:00:00','2017-01-31 21:00:00'),(8,178,'cash',8,0,1000000,0,'2547115709911','2017-02-27 00:00:00',2,'2017-02-26 21:00:00','2017-02-26 21:00:00');
/*!40000 ALTER TABLE `qp_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_ussd_activity`
--

DROP TABLE IF EXISTS `qp_ussd_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_ussd_activity` (
  `ussd_id` int(200) NOT NULL AUTO_INCREMENT,
  `session_id` varchar(200) NOT NULL,
  `phone_no` varchar(45) NOT NULL,
  `text` varchar(200) NOT NULL,
  `service_code` varchar(200) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ussd_id`)
) ENGINE=InnoDB AUTO_INCREMENT=326 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_ussd_activity`
--

LOCK TABLES `qp_ussd_activity` WRITE;
/*!40000 ALTER TABLE `qp_ussd_activity` DISABLE KEYS */;
INSERT INTO `qp_ussd_activity` VALUES (1,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','0','*483*789%23','2017-02-28 08:40:21','2017-02-28 08:40:21'),(2,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1','*483*789%23','2017-02-28 08:40:26','2017-02-28 08:40:26'),(3,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin','*483*789%23','2017-02-28 08:40:41','2017-02-28 08:40:41'),(4,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin*Admin','*483*789%23','2017-02-28 08:40:51','2017-02-28 08:40:51'),(5,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin*Admin*12345','*483*789%23','2017-02-28 08:40:56','2017-02-28 08:40:56'),(6,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin*Admin*12345*1111','*483*789%23','2017-02-28 08:41:05','2017-02-28 08:41:05'),(7,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin*Admin*12345*1111*1','*483*789%23','2017-02-28 08:41:12','2017-02-28 08:41:12'),(8,'ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','254714293809','1*Admin*Admin*12345*1111*1*00','*483*789%23','2017-02-28 08:41:25','2017-02-28 08:41:25'),(9,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','0','*483*789%23','2017-02-28 08:41:32','2017-02-28 08:41:32'),(10,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','1111','*483*789%23','2017-02-28 08:41:37','2017-02-28 08:41:37'),(11,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','0','*483*789%23','2017-03-01 12:52:43','2017-03-01 12:52:43'),(12,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','0','*483*789%23','2017-03-01 12:53:16','2017-03-01 12:53:16'),(13,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','0','*483*789%23','2017-03-01 13:01:01','2017-03-01 13:01:01'),(14,'ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','254714293809','6','*483*789%23','2017-03-01 13:01:07','2017-03-01 13:01:07'),(15,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','0','*483*789%23','2017-03-02 10:07:37','2017-03-02 10:07:37'),(16,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','1111','*483*789%23','2017-03-02 10:07:45','2017-03-02 10:07:45'),(17,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','1111*6','*483*789%23','2017-03-02 10:07:52','2017-03-02 10:07:52'),(18,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','0','*483*789%23','2017-03-02 10:08:50','2017-03-02 10:08:50'),(19,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-02 10:08:56','2017-03-02 10:08:56'),(20,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-02 10:09:02','2017-03-02 10:09:02'),(21,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00','*483*789%23','2017-03-02 10:11:06','2017-03-02 10:11:06'),(22,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6','*483*789%23','2017-03-02 10:11:11','2017-03-02 10:11:11'),(23,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00','*483*789%23','2017-03-02 10:11:44','2017-03-02 10:11:44'),(24,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00*6','*483*789%23','2017-03-02 10:11:48','2017-03-02 10:11:48'),(25,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00*6*00','*483*789%23','2017-03-02 10:13:46','2017-03-02 10:13:46'),(26,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00*6*00*00','*483*789%23','2017-03-02 10:19:36','2017-03-02 10:19:36'),(27,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00*6*00*00*6','*483*789%23','2017-03-02 10:20:03','2017-03-02 10:20:03'),(28,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-02 12:28:01','2017-03-02 12:28:01'),(29,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-02 12:28:08','2017-03-02 12:28:08'),(30,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00','*483*789%23','2017-03-02 12:30:18','2017-03-02 12:30:18'),(31,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6','*483*789%23','2017-03-02 12:30:23','2017-03-02 12:30:23'),(32,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00','*483*789%23','2017-03-02 12:33:22','2017-03-02 12:33:22'),(33,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*00*6*00*6','*483*789%23','2017-03-02 12:33:27','2017-03-02 12:33:27'),(34,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','0','*483*789%23','2017-03-02 13:37:19','2017-03-02 13:37:19'),(35,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-02 13:37:34','2017-03-02 13:37:34'),(36,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-02 13:37:38','2017-03-02 13:37:38'),(37,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 04:56:05','2017-03-06 04:56:05'),(38,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 04:56:11','2017-03-06 04:56:11'),(39,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 04:56:38','2017-03-06 04:56:38'),(40,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1','*483*789%23','2017-03-06 05:11:19','2017-03-06 05:11:19'),(41,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00','*483*789%23','2017-03-06 05:11:29','2017-03-06 05:11:29'),(42,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6','*483*789%23','2017-03-06 05:11:33','2017-03-06 05:11:33'),(43,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6*2','*483*789%23','2017-03-06 05:11:38','2017-03-06 05:11:38'),(44,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6*2*00','*483*789%23','2017-03-06 05:27:29','2017-03-06 05:27:29'),(45,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6*2*00*6','*483*789%23','2017-03-06 05:27:34','2017-03-06 05:27:34'),(46,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6*2*00*6*1','*483*789%23','2017-03-06 05:27:42','2017-03-06 05:27:42'),(47,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*1*00*6*2*00*6*1*Hashi+Oil','*483*789%23','2017-03-06 05:28:01','2017-03-06 05:28:01'),(48,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 06:51:59','2017-03-06 06:51:59'),(49,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 06:52:04','2017-03-06 06:52:04'),(50,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 06:52:08','2017-03-06 06:52:08'),(51,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil','*483*789%23','2017-03-06 06:52:18','2017-03-06 06:52:18'),(52,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 06:56:16','2017-03-06 06:56:16'),(53,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 06:56:20','2017-03-06 06:56:20'),(54,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 06:56:25','2017-03-06 06:56:25'),(55,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil','*483*789%23','2017-03-06 06:56:43','2017-03-06 06:56:43'),(56,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*RTFG445','*483*789%23','2017-03-06 06:57:02','2017-03-06 06:57:02'),(57,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 07:10:14','2017-03-06 07:10:14'),(58,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 07:10:18','2017-03-06 07:10:18'),(59,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 07:10:21','2017-03-06 07:10:21'),(60,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil','*483*789%23','2017-03-06 07:10:31','2017-03-06 07:10:31'),(61,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt5678','*483*789%23','2017-03-06 07:10:37','2017-03-06 07:10:37'),(62,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt5678*Rongai','*483*789%23','2017-03-06 07:10:44','2017-03-06 07:10:44'),(63,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt5678*Rongai*5567','*483*789%23','2017-03-06 07:10:51','2017-03-06 07:10:51'),(64,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 07:14:22','2017-03-06 07:14:22'),(65,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 07:14:26','2017-03-06 07:14:26'),(66,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 07:14:32','2017-03-06 07:14:32'),(67,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil','*483*789%23','2017-03-06 07:14:38','2017-03-06 07:14:38'),(68,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt4567','*483*789%23','2017-03-06 07:14:44','2017-03-06 07:14:44'),(69,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt4567*234+apollo','*483*789%23','2017-03-06 07:14:52','2017-03-06 07:14:52'),(70,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+Oil*rt4567*234+apollo*342+nanyuki','*483*789%23','2017-03-06 07:15:11','2017-03-06 07:15:11'),(71,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00','*483*789%23','2017-03-06 07:18:07','2017-03-06 07:18:07'),(72,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6','*483*789%23','2017-03-06 07:18:11','2017-03-06 07:18:11'),(73,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1','*483*789%23','2017-03-06 07:18:15','2017-03-06 07:18:15'),(74,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+oil','*483*789%23','2017-03-06 07:18:29','2017-03-06 07:18:29'),(75,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+oil*rt67','*483*789%23','2017-03-06 07:20:17','2017-03-06 07:20:17'),(76,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+oil*rt67*rongai','*483*789%23','2017-03-06 07:23:08','2017-03-06 07:23:08'),(77,'ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','254714293809','00*6*1*Hashi+oil*rt67*rongai*999','*483*789%23','2017-03-06 07:25:35','2017-03-06 07:25:35'),(78,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','0','*483*789%23','2017-03-06 07:29:19','2017-03-06 07:29:19'),(79,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111','*483*789%23','2017-03-06 07:29:30','2017-03-06 07:29:30'),(80,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6','*483*789%23','2017-03-06 07:29:36','2017-03-06 07:29:36'),(81,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1','*483*789%23','2017-03-06 07:29:43','2017-03-06 07:29:43'),(82,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai','*483*789%23','2017-03-06 07:29:52','2017-03-06 07:29:52'),(83,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345','*483*789%23','2017-03-06 07:30:03','2017-03-06 07:30:03'),(84,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai','*483*789%23','2017-03-06 07:30:11','2017-03-06 07:30:11'),(85,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234','*483*789%23','2017-03-06 07:30:18','2017-03-06 07:30:18'),(86,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1','*483*789%23','2017-03-06 07:30:28','2017-03-06 07:30:28'),(87,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00','*483*789%23','2017-03-06 07:37:35','2017-03-06 07:37:35'),(88,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6','*483*789%23','2017-03-06 07:37:42','2017-03-06 07:37:42'),(89,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1','*483*789%23','2017-03-06 07:37:45','2017-03-06 07:37:45'),(90,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254','*483*789%23','2017-03-06 07:37:56','2017-03-06 07:37:56'),(91,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254*rt4567','*483*789%23','2017-03-06 07:38:01','2017-03-06 07:38:01'),(92,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254*rt4567*Rongai','*483*789%23','2017-03-06 07:38:07','2017-03-06 07:38:07'),(93,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','0','*483*789%23','2017-03-06 07:38:50','2017-03-06 07:38:50'),(94,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1','*483*789%23','2017-03-06 07:38:56','2017-03-06 07:38:56'),(95,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00','*483*789%23','2017-03-06 07:39:22','2017-03-06 07:39:22'),(96,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6','*483*789%23','2017-03-06 07:39:26','2017-03-06 07:39:26'),(97,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1','*483*789%23','2017-03-06 07:39:30','2017-03-06 07:39:30'),(98,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1*Grid+254','*483*789%23','2017-03-06 07:39:37','2017-03-06 07:39:37'),(99,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1*Grid+254*rt4567','*483*789%23','2017-03-06 07:39:43','2017-03-06 07:39:43'),(100,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1*Grid+254*rt4567*Rongai','*483*789%23','2017-03-06 07:39:49','2017-03-06 07:39:49'),(101,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1*Grid+254*rt4567*Rongai*768','*483*789%23','2017-03-06 07:39:54','2017-03-06 07:39:54'),(102,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','1*00*6*1*Grid+254*rt4567*Rongai*768*1','*483*789%23','2017-03-06 07:40:10','2017-03-06 07:40:10'),(103,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 07:40:45','2017-03-06 07:40:45'),(104,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 07:40:48','2017-03-06 07:40:48'),(105,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 07:40:52','2017-03-06 07:40:52'),(106,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Grid254','*483*789%23','2017-03-06 07:40:59','2017-03-06 07:40:59'),(107,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Grid254*rt5678','*483*789%23','2017-03-06 07:41:03','2017-03-06 07:41:03'),(108,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Grid254*rt5678*Rongai','*483*789%23','2017-03-06 07:41:14','2017-03-06 07:41:14'),(109,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Grid254*rt5678*Rongai*234','*483*789%23','2017-03-06 07:41:19','2017-03-06 07:41:19'),(110,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Grid254*rt5678*Rongai*234*1','*483*789%23','2017-03-06 07:41:23','2017-03-06 07:41:23'),(111,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:00:38','2017-03-06 08:00:38'),(112,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:00:46','2017-03-06 08:00:46'),(113,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 08:00:55','2017-03-06 08:00:55'),(114,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai','*483*789%23','2017-03-06 08:01:02','2017-03-06 08:01:02'),(115,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555','*483*789%23','2017-03-06 08:01:14','2017-03-06 08:01:14'),(116,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556','*483*789%23','2017-03-06 08:03:54','2017-03-06 08:03:54'),(117,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00','*483*789%23','2017-03-06 08:04:19','2017-03-06 08:04:19'),(118,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6','*483*789%23','2017-03-06 08:04:23','2017-03-06 08:04:23'),(119,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6*1','*483*789%23','2017-03-06 08:04:29','2017-03-06 08:04:29'),(120,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala','*483*789%23','2017-03-06 08:04:34','2017-03-06 08:04:34'),(121,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678','*483*789%23','2017-03-06 08:04:43','2017-03-06 08:04:43'),(122,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678*Rongai','*483*789%23','2017-03-06 08:04:51','2017-03-06 08:04:51'),(123,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678*Rongai*778','*483*789%23','2017-03-06 08:04:56','2017-03-06 08:04:56'),(124,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:12:26','2017-03-06 08:12:26'),(125,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:12:31','2017-03-06 08:12:31'),(126,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 08:12:36','2017-03-06 08:12:36'),(127,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala','*483*789%23','2017-03-06 08:12:41','2017-03-06 08:12:41'),(128,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*123rt56','*483*789%23','2017-03-06 08:12:48','2017-03-06 08:12:48'),(129,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*123rt56*Rongai','*483*789%23','2017-03-06 08:12:54','2017-03-06 08:12:54'),(130,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*123rt56*Rongai*234','*483*789%23','2017-03-06 08:13:01','2017-03-06 08:13:01'),(131,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*123rt56*Rongai*234*1','*483*789%23','2017-03-06 08:13:15','2017-03-06 08:13:15'),(132,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:16:21','2017-03-06 08:16:21'),(133,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:16:25','2017-03-06 08:16:25'),(134,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 08:16:30','2017-03-06 08:16:30'),(135,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala','*483*789%23','2017-03-06 08:16:37','2017-03-06 08:16:37'),(136,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt56789','*483*789%23','2017-03-06 08:16:42','2017-03-06 08:16:42'),(137,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt56789*Rongai','*483*789%23','2017-03-06 08:16:51','2017-03-06 08:16:51'),(138,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt56789*Rongai*7865','*483*789%23','2017-03-06 08:16:56','2017-03-06 08:16:56'),(139,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt56789*Rongai*7865*1','*483*789%23','2017-03-06 08:17:04','2017-03-06 08:17:04'),(140,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:33:58','2017-03-06 08:33:58'),(141,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:34:02','2017-03-06 08:34:02'),(142,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 08:34:07','2017-03-06 08:34:07'),(143,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala','*483*789%23','2017-03-06 08:34:13','2017-03-06 08:34:13'),(144,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt5463','*483*789%23','2017-03-06 08:34:21','2017-03-06 08:34:21'),(145,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt5463*Rongai','*483*789%23','2017-03-06 08:34:26','2017-03-06 08:34:26'),(146,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt5463*Rongai*546','*483*789%23','2017-03-06 08:34:31','2017-03-06 08:34:31'),(147,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Koala*rt5463*Rongai*546*1','*483*789%23','2017-03-06 08:34:46','2017-03-06 08:34:46'),(148,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:35:11','2017-03-06 08:35:11'),(149,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:35:15','2017-03-06 08:35:15'),(150,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 08:35:22','2017-03-06 08:35:22'),(151,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Fintech','*483*789%23','2017-03-06 08:35:33','2017-03-06 08:35:33'),(152,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Fintech*rt5678','*483*789%23','2017-03-06 08:35:40','2017-03-06 08:35:40'),(153,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Fintech*rt5678*Karen','*483*789%23','2017-03-06 08:35:46','2017-03-06 08:35:46'),(154,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Fintech*rt5678*Karen*123','*483*789%23','2017-03-06 08:35:51','2017-03-06 08:35:51'),(155,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Fintech*rt5678*Karen*123*1','*483*789%23','2017-03-06 08:37:25','2017-03-06 08:37:25'),(156,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:41:20','2017-03-06 08:41:20'),(157,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:41:25','2017-03-06 08:41:25'),(158,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:53:15','2017-03-06 08:53:15'),(159,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:53:20','2017-03-06 08:53:20'),(160,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 08:56:48','2017-03-06 08:56:48'),(161,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 08:56:53','2017-03-06 08:56:53'),(162,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:00:33','2017-03-06 09:00:33'),(163,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:00:37','2017-03-06 09:00:37'),(164,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','0','*483*789%23','2017-03-06 09:01:18','2017-03-06 09:01:18'),(165,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:01:24','2017-03-06 09:01:24'),(166,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:01:28','2017-03-06 09:01:28'),(167,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:03:44','2017-03-06 09:03:44'),(168,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:03:48','2017-03-06 09:03:48'),(169,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:07:35','2017-03-06 09:07:35'),(170,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:07:39','2017-03-06 09:07:39'),(171,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:16:07','2017-03-06 09:16:07'),(172,'ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:17:08','2017-03-06 09:17:08'),(173,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','0','*483*789%23','2017-03-06 09:19:29','2017-03-06 09:19:29'),(174,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','0','*483*789%23','2017-03-06 09:21:56','2017-03-06 09:21:56'),(175,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111','*483*789%23','2017-03-06 09:22:01','2017-03-06 09:22:01'),(176,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6','*483*789%23','2017-03-06 09:22:06','2017-03-06 09:22:06'),(177,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1','*483*789%23','2017-03-06 09:22:12','2017-03-06 09:22:12'),(178,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Qopah','*483*789%23','2017-03-06 09:22:18','2017-03-06 09:22:18'),(179,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Qopah*rt5463','*483*789%23','2017-03-06 09:22:23','2017-03-06 09:22:23'),(180,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Qopah*rt5463*Rongai','*483*789%23','2017-03-06 09:22:30','2017-03-06 09:22:30'),(181,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Qopah*rt5463*Rongai*5678','*483*789%23','2017-03-06 09:22:34','2017-03-06 09:22:34'),(182,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','1111*6*1*Qopah*rt5463*Rongai*5678*1','*483*789%23','2017-03-06 09:22:40','2017-03-06 09:22:40'),(183,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:24:41','2017-03-06 09:24:41'),(184,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:24:46','2017-03-06 09:24:46'),(185,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 09:24:49','2017-03-06 09:24:49'),(186,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah','*483*789%23','2017-03-06 09:24:55','2017-03-06 09:24:55'),(187,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5678','*483*789%23','2017-03-06 09:25:00','2017-03-06 09:25:00'),(188,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5678*Rongai','*483*789%23','2017-03-06 09:25:06','2017-03-06 09:25:06'),(189,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5678*Rongai*768','*483*789%23','2017-03-06 09:25:10','2017-03-06 09:25:10'),(190,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5678*Rongai*768*1','*483*789%23','2017-03-06 09:25:16','2017-03-06 09:25:16'),(191,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5678*Rongai*768*1*1','*483*789%23','2017-03-06 09:27:12','2017-03-06 09:27:12'),(192,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:34:00','2017-03-06 09:34:00'),(193,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:34:04','2017-03-06 09:34:04'),(194,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 09:34:07','2017-03-06 09:34:07'),(195,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah','*483*789%23','2017-03-06 09:34:15','2017-03-06 09:34:15'),(196,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt67543','*483*789%23','2017-03-06 09:34:21','2017-03-06 09:34:21'),(197,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt67543*KAren','*483*789%23','2017-03-06 09:34:27','2017-03-06 09:34:27'),(198,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt67543*KAren*2345','*483*789%23','2017-03-06 09:34:33','2017-03-06 09:34:33'),(199,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt67543*KAren*2345*1','*483*789%23','2017-03-06 09:34:41','2017-03-06 09:34:41'),(200,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:36:12','2017-03-06 09:36:12'),(201,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:36:15','2017-03-06 09:36:15'),(202,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 09:36:22','2017-03-06 09:36:22'),(203,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah','*483*789%23','2017-03-06 09:36:49','2017-03-06 09:36:49'),(204,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt56789','*483*789%23','2017-03-06 09:36:54','2017-03-06 09:36:54'),(205,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt56789*Rongai','*483*789%23','2017-03-06 09:37:09','2017-03-06 09:37:09'),(206,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt56789*Rongai*342','*483*789%23','2017-03-06 09:37:14','2017-03-06 09:37:14'),(207,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt56789*Rongai*342*1','*483*789%23','2017-03-06 09:37:34','2017-03-06 09:37:34'),(208,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:40:24','2017-03-06 09:40:24'),(209,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:40:29','2017-03-06 09:40:29'),(210,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 09:40:36','2017-03-06 09:40:36'),(211,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah','*483*789%23','2017-03-06 09:40:49','2017-03-06 09:40:49'),(212,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5453','*483*789%23','2017-03-06 09:40:55','2017-03-06 09:40:55'),(213,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5453*Rongai','*483*789%23','2017-03-06 09:41:02','2017-03-06 09:41:02'),(214,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5453*Rongai*234','*483*789%23','2017-03-06 09:41:07','2017-03-06 09:41:07'),(215,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah*rt5453*Rongai*234*1','*483*789%23','2017-03-06 09:41:14','2017-03-06 09:41:14'),(216,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-06 09:45:01','2017-03-06 09:45:01'),(217,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-06 09:45:05','2017-03-06 09:45:05'),(218,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-06 09:45:20','2017-03-06 09:45:20'),(219,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Services','*483*789%23','2017-03-06 09:45:30','2017-03-06 09:45:30'),(220,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Services*rt5463','*483*789%23','2017-03-06 09:45:37','2017-03-06 09:45:37'),(221,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Services*rt5463*Karen','*483*789%23','2017-03-06 09:45:42','2017-03-06 09:45:42'),(222,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Services*rt5463*Karen*342','*483*789%23','2017-03-06 09:45:47','2017-03-06 09:45:47'),(223,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*Qopah+Services*rt5463*Karen*342*1','*483*789%23','2017-03-06 09:46:00','2017-03-06 09:46:00'),(224,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 06:07:50','2017-03-07 06:07:50'),(225,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 06:07:57','2017-03-07 06:07:57'),(226,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00','*483*789%23','2017-03-07 06:09:21','2017-03-07 06:09:21'),(227,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00*6','*483*789%23','2017-03-07 06:09:25','2017-03-07 06:09:25'),(228,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00*6*1','*483*789%23','2017-03-07 06:09:31','2017-03-07 06:09:31'),(229,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00*6*1*00','*483*789%23','2017-03-07 06:11:32','2017-03-07 06:11:32'),(230,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00*6*1*00*6','*483*789%23','2017-03-07 06:11:40','2017-03-07 06:11:40'),(231,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*00*6*1*00*6*1','*483*789%23','2017-03-07 06:11:44','2017-03-07 06:11:44'),(232,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 06:18:25','2017-03-07 06:18:25'),(233,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 06:18:30','2017-03-07 06:18:30'),(234,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 06:18:34','2017-03-07 06:18:34'),(235,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*6000','*483*789%23','2017-03-07 06:37:57','2017-03-07 06:37:57'),(236,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 06:44:41','2017-03-07 06:44:41'),(237,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 06:44:46','2017-03-07 06:44:46'),(238,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 06:44:52','2017-03-07 06:44:52'),(239,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*80','*483*789%23','2017-03-07 06:45:01','2017-03-07 06:45:01'),(240,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 07:19:56','2017-03-07 07:19:56'),(241,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 07:20:01','2017-03-07 07:20:01'),(242,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 07:20:05','2017-03-07 07:20:05'),(243,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*100000','*483*789%23','2017-03-07 07:20:18','2017-03-07 07:20:18'),(244,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*100000*4','*483*789%23','2017-03-07 07:20:25','2017-03-07 07:20:25'),(245,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*100000*4*1','*483*789%23','2017-03-07 07:20:35','2017-03-07 07:20:35'),(246,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*100000*4*1*1','*483*789%23','2017-03-07 07:20:56','2017-03-07 07:20:56'),(247,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 07:41:52','2017-03-07 07:41:52'),(248,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 07:41:57','2017-03-07 07:41:57'),(249,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 07:42:03','2017-03-07 07:42:03'),(250,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*3000000','*483*789%23','2017-03-07 07:42:11','2017-03-07 07:42:11'),(251,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*3000000*1','*483*789%23','2017-03-07 07:42:18','2017-03-07 07:42:18'),(252,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*3000000*1*2','*483*789%23','2017-03-07 07:42:53','2017-03-07 07:42:53'),(253,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*3000000*1*2*1','*483*789%23','2017-03-07 07:42:58','2017-03-07 07:42:58'),(254,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 07:59:11','2017-03-07 07:59:11'),(255,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 07:59:45','2017-03-07 07:59:45'),(256,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 07:59:49','2017-03-07 07:59:49'),(257,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 07:59:52','2017-03-07 07:59:52'),(258,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-07 07:59:56','2017-03-07 07:59:56'),(259,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000','*483*789%23','2017-03-07 08:01:21','2017-03-07 08:01:21'),(260,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*1','*483*789%23','2017-03-07 08:03:49','2017-03-07 08:03:49'),(261,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*1*1','*483*789%23','2017-03-07 08:05:36','2017-03-07 08:05:36'),(262,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 08:13:59','2017-03-07 08:13:59'),(263,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 08:14:04','2017-03-07 08:14:04'),(264,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 08:14:11','2017-03-07 08:14:11'),(265,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-07 08:14:16','2017-03-07 08:14:16'),(266,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*3456666','*483*789%23','2017-03-07 08:14:23','2017-03-07 08:14:23'),(267,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*3456666*2','*483*789%23','2017-03-07 08:14:27','2017-03-07 08:14:27'),(268,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*3456666*2*1','*483*789%23','2017-03-07 08:14:35','2017-03-07 08:14:35'),(269,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 12:00:05','2017-03-07 12:00:05'),(270,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 12:00:21','2017-03-07 12:00:21'),(271,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 12:00:26','2017-03-07 12:00:26'),(272,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-07 12:00:33','2017-03-07 12:00:33'),(273,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000','*483*789%23','2017-03-07 12:00:43','2017-03-07 12:00:43'),(274,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*4','*483*789%23','2017-03-07 12:00:52','2017-03-07 12:00:52'),(275,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*4*1','*483*789%23','2017-03-07 12:01:00','2017-03-07 12:01:00'),(276,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 13:13:05','2017-03-07 13:13:05'),(277,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 13:13:09','2017-03-07 13:13:09'),(278,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 13:13:12','2017-03-07 13:13:12'),(279,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-07 13:13:17','2017-03-07 13:13:17'),(280,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000','*483*789%23','2017-03-07 13:13:22','2017-03-07 13:13:22'),(281,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*1','*483*789%23','2017-03-07 13:13:27','2017-03-07 13:13:27'),(282,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*20000*1*1','*483*789%23','2017-03-07 13:13:42','2017-03-07 13:13:42'),(283,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 13:23:24','2017-03-07 13:23:24'),(284,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 13:23:27','2017-03-07 13:23:27'),(285,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 13:23:32','2017-03-07 13:23:32'),(286,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2','*483*789%23','2017-03-07 13:23:41','2017-03-07 13:23:41'),(287,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000','*483*789%23','2017-03-07 13:23:49','2017-03-07 13:23:49'),(288,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5','*483*789%23','2017-03-07 13:23:53','2017-03-07 13:23:53'),(289,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2','*483*789%23','2017-03-07 13:23:58','2017-03-07 13:23:58'),(290,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1','*483*789%23','2017-03-07 13:24:03','2017-03-07 13:24:03'),(291,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00','*483*789%23','2017-03-07 13:27:05','2017-03-07 13:27:05'),(292,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6','*483*789%23','2017-03-07 13:27:10','2017-03-07 13:27:10'),(293,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1','*483*789%23','2017-03-07 13:27:13','2017-03-07 13:27:13'),(294,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1*2','*483*789%23','2017-03-07 13:27:17','2017-03-07 13:27:17'),(295,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1*2*90000','*483*789%23','2017-03-07 13:27:23','2017-03-07 13:27:23'),(296,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1','*483*789%23','2017-03-07 13:27:27','2017-03-07 13:27:27'),(297,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1*2','*483*789%23','2017-03-07 13:27:32','2017-03-07 13:27:32'),(298,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1*2*1','*483*789%23','2017-03-07 13:27:40','2017-03-07 13:27:40'),(299,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-07 13:29:18','2017-03-07 13:29:18'),(300,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-07 13:29:23','2017-03-07 13:29:23'),(301,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-07 13:29:28','2017-03-07 13:29:28'),(302,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2','*483*789%23','2017-03-07 13:29:39','2017-03-07 13:29:39'),(303,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*4000000','*483*789%23','2017-03-07 13:29:46','2017-03-07 13:29:46'),(304,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*4000000*4','*483*789%23','2017-03-07 13:29:55','2017-03-07 13:29:55'),(305,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*4000000*4*2','*483*789%23','2017-03-07 13:30:06','2017-03-07 13:30:06'),(306,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*4000000*4*2*1','*483*789%23','2017-03-07 13:31:07','2017-03-07 13:31:07'),(307,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*2*4000000*4*2*1*1','*483*789%23','2017-03-07 13:31:13','2017-03-07 13:31:13'),(308,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-08 10:54:08','2017-03-08 10:54:08'),(309,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-08 10:54:13','2017-03-08 10:54:13'),(310,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-08 10:54:16','2017-03-08 10:54:16'),(311,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-08 10:54:20','2017-03-08 10:54:20'),(312,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*900000','*483*789%23','2017-03-08 10:54:25','2017-03-08 10:54:25'),(313,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*900000*3','*483*789%23','2017-03-08 10:54:31','2017-03-08 10:54:31'),(314,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*900000*3*1','*483*789%23','2017-03-08 10:54:37','2017-03-08 10:54:37'),(315,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*900000*3*1*1','*483*789%23','2017-03-08 10:54:43','2017-03-08 10:54:43'),(316,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00','*483*789%23','2017-03-08 10:58:18','2017-03-08 10:58:18'),(317,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6','*483*789%23','2017-03-08 10:58:23','2017-03-08 10:58:23'),(318,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1','*483*789%23','2017-03-08 10:58:27','2017-03-08 10:58:27'),(319,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1','*483*789%23','2017-03-08 10:58:31','2017-03-08 10:58:31'),(320,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*90000','*483*789%23','2017-03-08 10:58:38','2017-03-08 10:58:38'),(321,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*90000*4','*483*789%23','2017-03-08 10:58:44','2017-03-08 10:58:44'),(322,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*90000*4*2','*483*789%23','2017-03-08 10:58:51','2017-03-08 10:58:51'),(323,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*90000*4*2*1','*483*789%23','2017-03-08 10:58:57','2017-03-08 10:58:57'),(324,'ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','254714293809','00*6*1*1*90000*4*2*1','*483*789%23','2017-03-08 11:00:44','2017-03-08 11:00:44'),(325,'ATUid_7897879xwqddddrd','254714293809','testing..','*483*789#','2017-03-09 13:33:38','2017-03-09 13:33:38');
/*!40000 ALTER TABLE `qp_ussd_activity` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-10 16:51:34
