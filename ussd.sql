-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: ussd
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `qp_activity_lookup`
--

DROP TABLE IF EXISTS `qp_activity_lookup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_activity_lookup` (
  `activity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(100) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_activity_lookup`
--

LOCK TABLES `qp_activity_lookup` WRITE;
/*!40000 ALTER TABLE `qp_activity_lookup` DISABLE KEYS */;
INSERT INTO `qp_activity_lookup` VALUES (1,'ussd','New session started'),(2,'ussd','New client registration started'),(3,'ussd','Meter Number added for the client'),(4,'ussd','ID/PIN Number added for the client'),(5,'ussd','Startup PIN created'),(6,'ussd','Startup PIN, Terms & Conditions sent to client'),(7,'ussd','Borrowed Electricity Token Sent to client'),(8,'ussd','New PIN number updated by the client'),(9,'ussd','Meter number updated by the client'),(10,'ussd','Meter number removed by the client'),(11,'ussd','Client updated their full name'),(12,'ussd','Client updated their email address'),(13,'ussd','Client updated their language'),(14,'ussd','Client logged out of the system'),(15,'ussd','Client has been blocked from using the system'),(16,'ussd','Purchased Electricity Token Sent to client');
/*!40000 ALTER TABLE `qp_activity_lookup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_airtime`
--

DROP TABLE IF EXISTS `qp_airtime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_airtime` (
  `airtime_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `client_id` int(11) unsigned NOT NULL,
  `debt_amount` float NOT NULL,
  `airtime_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`airtime_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_airtime`
--

LOCK TABLES `qp_airtime` WRITE;
/*!40000 ALTER TABLE `qp_airtime` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_airtime` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_archived_tokens`
--

DROP TABLE IF EXISTS `qp_archived_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_archived_tokens` (
  `token_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `token_name` varchar(255) NOT NULL,
  `token_worth` float NOT NULL,
  `token_units` float NOT NULL,
  `token_serial_number` varchar(255) NOT NULL,
  `expiry_date` date NOT NULL,
  `token_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_archived_tokens`
--

LOCK TABLES `qp_archived_tokens` WRITE;
/*!40000 ALTER TABLE `qp_archived_tokens` DISABLE KEYS */;
INSERT INTO `qp_archived_tokens` VALUES (1,'1256365895465425',350,21,'SD472L09R','2017-05-20',1,'2016-03-18 00:00:00','2016-03-18 00:00:00'),(2,'8956321478525556',500,26,'HY78T5432K','2017-04-30',1,'2016-03-01 00:00:00','2016-03-01 00:00:00');
/*!40000 ALTER TABLE `qp_archived_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_cash`
--

DROP TABLE IF EXISTS `qp_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_cash` (
  `cash_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `client_id` int(11) unsigned NOT NULL,
  `debt_amount` float NOT NULL,
  `cash_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cash_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_cash`
--

LOCK TABLES `qp_cash` WRITE;
/*!40000 ALTER TABLE `qp_cash` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_city`
--

DROP TABLE IF EXISTS `qp_city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_city` (
  `city_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_id` int(11) unsigned NOT NULL,
  `city_name` varchar(100) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_city`
--

LOCK TABLES `qp_city` WRITE;
/*!40000 ALTER TABLE `qp_city` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_client_profiling`
--

DROP TABLE IF EXISTS `qp_client_profiling`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_client_profiling` (
  `profiling_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `client_id` int(11) unsigned NOT NULL,
  `meter_number` varchar(255) NOT NULL,
  `weekly_usage` text NOT NULL,
  `token_value_one` int(11) unsigned NOT NULL,
  `token_value_two` int(11) unsigned NOT NULL,
  `token_value_three` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`profiling_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_client_profiling`
--

LOCK TABLES `qp_client_profiling` WRITE;
/*!40000 ALTER TABLE `qp_client_profiling` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_client_profiling` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_client_status`
--

DROP TABLE IF EXISTS `qp_client_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_client_status` (
  `status_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `status_name` varchar(50) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_client_status`
--

LOCK TABLES `qp_client_status` WRITE;
/*!40000 ALTER TABLE `qp_client_status` DISABLE KEYS */;
INSERT INTO `qp_client_status` VALUES (1,'Registering'),(2,'Registered'),(3,'Active'),(4,'Over Limit'),(5,'Past Deadline'),(6,'Suspended'),(7,'Defaulted'),(8,'Blocked');
/*!40000 ALTER TABLE `qp_client_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_clients`
--

DROP TABLE IF EXISTS `qp_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_clients` (
  `client_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `full_name` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `pin_number` varchar(100) NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `meter_number` varchar(255) NOT NULL,
  `startup_ussd_pin` int(11) unsigned NOT NULL,
  `pin_sent` int(10) unsigned NOT NULL,
  `registration_step` int(10) unsigned NOT NULL,
  `id_number` varchar(100) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `account_status` int(11) unsigned NOT NULL,
  `temporary_pin` varchar(100) NOT NULL,
  `temporary_meter` varchar(100) NOT NULL,
  `temporary_name` varchar(255) NOT NULL,
  `temporary_email` varchar(255) NOT NULL,
  `temporary_language` int(10) unsigned NOT NULL,
  `temporary_purchase` varchar(100) NOT NULL,
  `trial_number` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `last_access` datetime NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_clients`
--

LOCK TABLES `qp_clients` WRITE;
/*!40000 ALTER TABLE `qp_clients` DISABLE KEYS */;
INSERT INTO `qp_clients` VALUES (117,'Admin Admin','254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','Dr7Bn+4jjA9laP1Tf47cQw==',1,'',0,0,97,'12345','254714293809',1,'Dr7Bn+4jjA9laP1Tf47cQw==','','Admin Admin','254714293809',1,'12345',1,'2017-02-28 11:41:12','2017-02-28 11:41:12','2017-02-28 11:41:12');
/*!40000 ALTER TABLE `qp_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_countries`
--

DROP TABLE IF EXISTS `qp_countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_countries` (
  `country_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `country_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_countries`
--

LOCK TABLES `qp_countries` WRITE;
/*!40000 ALTER TABLE `qp_countries` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_electricity_meter`
--

DROP TABLE IF EXISTS `qp_electricity_meter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_electricity_meter` (
  `meter_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `client_id` int(11) unsigned NOT NULL,
  `meter_number` varchar(255) NOT NULL,
  `debt_amount` float NOT NULL,
  `meter_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`meter_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_electricity_meter`
--

LOCK TABLES `qp_electricity_meter` WRITE;
/*!40000 ALTER TABLE `qp_electricity_meter` DISABLE KEYS */;
INSERT INTO `qp_electricity_meter` VALUES (1,151,'123456',300,1,'2016-12-14 14:27:01','2016-12-14 14:27:01'),(2,152,'99999',10,1,'2016-12-14 21:02:51','2016-12-14 21:02:51'),(3,152,'99999',10,1,'2016-12-14 21:02:54','2016-12-14 21:02:54'),(4,153,'123456789',10,1,'2016-12-19 15:06:43','2016-12-19 15:06:43'),(5,153,'123456789',30,1,'2016-12-19 15:23:18','2016-12-19 15:23:18'),(6,156,'12344321',10,1,'2016-12-27 20:41:45','2016-12-27 20:41:45'),(7,159,'223344',10,1,'2016-12-30 22:16:51','2016-12-30 22:16:51'),(8,162,'123456',10,1,'2017-01-10 08:05:40','2017-01-10 08:05:40'),(9,162,'123456',10,1,'2017-01-10 08:06:32','2017-01-10 08:06:32'),(10,162,'123456',10,1,'2017-01-10 09:11:32','2017-01-10 09:11:32'),(11,162,'123456',20,1,'2017-01-10 09:11:58','2017-01-10 09:11:58'),(12,162,'123456',10,1,'2017-01-10 09:14:41','2017-01-10 09:14:41'),(13,162,'123456',10,1,'2017-01-10 09:25:06','2017-01-10 09:25:06'),(14,162,'123456',20,1,'2017-01-10 09:53:24','2017-01-10 09:53:24'),(15,162,'123456',40,1,'2017-01-10 10:08:00','2017-01-10 10:08:00'),(16,162,'123456',40,1,'2017-01-10 10:10:12','2017-01-10 10:10:12'),(17,162,'123456',40,1,'2017-01-10 10:15:22','2017-01-10 10:15:22'),(18,162,'123456',30,1,'2017-01-10 10:24:24','2017-01-10 10:24:24'),(19,162,'123456',30,1,'2017-01-10 10:24:50','2017-01-10 10:24:50'),(20,162,'123456',40,1,'2017-01-10 10:28:26','2017-01-10 10:28:26'),(21,162,'123456',40,1,'2017-01-10 11:01:07','2017-01-10 11:01:07'),(22,162,'123456',40,1,'2017-01-10 11:10:29','2017-01-10 11:10:29'),(23,162,'123456',20,1,'2017-01-10 11:16:18','2017-01-10 11:16:18'),(24,162,'123456',40,1,'2017-01-10 11:23:58','2017-01-10 11:23:58'),(25,162,'123456',40,1,'2017-01-10 11:25:36','2017-01-10 11:25:36'),(26,162,'123456',40,1,'2017-01-10 11:27:02','2017-01-10 11:27:02'),(27,162,'123456',40,1,'2017-01-10 11:29:00','2017-01-10 11:29:00'),(28,162,'123456',40,1,'2017-01-10 11:29:28','2017-01-10 11:29:28'),(29,162,'123456',40,1,'2017-01-10 11:35:19','2017-01-10 11:35:19'),(30,162,'123456',10,1,'2017-01-10 11:38:12','2017-01-10 11:38:12'),(31,162,'123456',20,1,'2017-01-10 11:39:14','2017-01-10 11:39:14'),(32,162,'123456',20,1,'2017-01-10 11:39:46','2017-01-10 11:39:46'),(33,162,'123456',10,1,'2017-01-10 11:41:15','2017-01-10 11:41:15'),(34,162,'123456',10,1,'2017-01-10 11:41:36','2017-01-10 11:41:36'),(35,162,'123456',40,1,'2017-01-10 16:20:05','2017-01-10 16:20:05'),(36,162,'123456',10,1,'2017-01-10 16:20:58','2017-01-10 16:20:58'),(37,162,'123456',20,1,'2017-01-10 16:24:26','2017-01-10 16:24:26'),(38,162,'123456',40,1,'2017-01-10 16:37:20','2017-01-10 16:37:20'),(39,162,'123456',10,1,'2017-01-10 16:58:45','2017-01-10 16:58:45'),(40,162,'123456',20,1,'2017-01-10 17:03:51','2017-01-10 17:03:51'),(41,162,'123456',10,1,'2017-01-10 17:06:39','2017-01-10 17:06:39'),(42,162,'123456',20,1,'2017-01-10 17:16:49','2017-01-10 17:16:49'),(43,162,'123456',10,1,'2017-01-25 11:15:51','2017-01-25 11:15:51'),(44,162,'123456',10,1,'2017-01-30 10:18:49','2017-01-30 10:18:49'),(45,162,'123456',10,1,'2017-01-30 10:21:09','2017-01-30 10:21:09'),(46,162,'123456',10,1,'2017-01-30 10:38:00','2017-01-30 10:38:00'),(47,162,'123456',20,1,'2017-02-02 08:01:35','2017-02-02 08:01:35'),(48,162,'123456',20,1,'2017-02-02 08:08:16','2017-02-02 08:08:16'),(49,162,'123456',40,1,'2017-02-02 08:31:43','2017-02-02 08:31:43'),(50,162,'123456',20,1,'2017-02-02 08:47:04','2017-02-02 08:47:04');
/*!40000 ALTER TABLE `qp_electricity_meter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_groups`
--

DROP TABLE IF EXISTS `qp_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_groups`
--

LOCK TABLES `qp_groups` WRITE;
/*!40000 ALTER TABLE `qp_groups` DISABLE KEYS */;
INSERT INTO `qp_groups` VALUES (1,'superadmin','Super Administrator'),(2,'admin','Administrator'),(3,'users','Normal System Users'),(4,'auditors','External Auditors');
/*!40000 ALTER TABLE `qp_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_inactive_clients`
--

DROP TABLE IF EXISTS `qp_inactive_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_inactive_clients` (
  `client_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `full_name` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `pin_number` varchar(100) NOT NULL,
  `language_id` int(10) unsigned NOT NULL,
  `meter_number` varchar(255) NOT NULL,
  `startup_ussd_pin` int(11) unsigned NOT NULL,
  `pin_sent` int(10) unsigned NOT NULL,
  `registration_step` int(10) unsigned NOT NULL,
  `id_number` varchar(100) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `account_status` int(11) unsigned NOT NULL,
  `temporary_pin` varchar(100) NOT NULL,
  `temporary_meter` varchar(100) NOT NULL,
  `temporary_name` varchar(255) NOT NULL,
  `temporary_email` varchar(255) NOT NULL,
  `temporary_language` int(10) unsigned NOT NULL,
  `trial_number` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `last_access` datetime NOT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_inactive_clients`
--

LOCK TABLES `qp_inactive_clients` WRITE;
/*!40000 ALTER TABLE `qp_inactive_clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_inactive_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_languages`
--

DROP TABLE IF EXISTS `qp_languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_languages` (
  `language_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `language_name` varchar(255) NOT NULL,
  `language_alias` varchar(3) NOT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_languages`
--

LOCK TABLES `qp_languages` WRITE;
/*!40000 ALTER TABLE `qp_languages` DISABLE KEYS */;
INSERT INTO `qp_languages` VALUES (1,'English','en'),(2,'Portuguese','pt'),(3,'Creole','cr');
/*!40000 ALTER TABLE `qp_languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_login_attempts`
--

DROP TABLE IF EXISTS `qp_login_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_login_attempts`
--

LOCK TABLES `qp_login_attempts` WRITE;
/*!40000 ALTER TABLE `qp_login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_login_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_outbox`
--

DROP TABLE IF EXISTS `qp_outbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_outbox` (
  `outbox_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `client_id` int(10) unsigned NOT NULL,
  `messageId` text NOT NULL,
  `message` text NOT NULL,
  `status` text NOT NULL,
  `sessionId` text NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`outbox_id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_outbox`
--

LOCK TABLES `qp_outbox` WRITE;
/*!40000 ALTER TABLE `qp_outbox` DISABLE KEYS */;
INSERT INTO `qp_outbox` VALUES (24,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Insufficient Balance','ATUid_7897879','2016-11-28 07:45:05'),(25,78,'None','Successfully added Meter No. 2345678 to your account.','Insufficient Balance','ATUid_7897879','2016-11-28 07:49:37'),(26,78,'None','Your request for KES 100 was declined due to inactive usage. Contact our help desk on 0205855861.','Insufficient Balance','ATUid_78978799','2016-11-28 07:55:15'),(27,78,'None','Your request for KES 100 was declined due to inactive usage. Contact our help desk on 0205855861.','Insufficient Balance','ATUid_78978799','2016-11-28 07:55:45'),(28,78,'None','Your request for KES 100 was declined due to inactive usage. Contact our help desk on 0205855861.','Insufficient Balance','ATUid_78978799','2016-11-28 07:55:51'),(29,78,'None','Your request for KES 100 was declined due to inactive usage. Contact our help desk on 0205855861.','Insufficient Balance','ATUid_78978799','2016-11-28 07:56:39'),(30,78,'None','Your request for KES 100 was declined due to inactive usage. Contact our help desk on 0205855861.','Insufficient Balance','ATUid_78978799','2016-11-28 07:57:01'),(31,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Insufficient Balance','ATUid_78978791','2016-11-28 10:05:59'),(32,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Insufficient Balance','ATUid_78957o8749212daso54e','2016-11-28 12:46:16'),(33,113,'None','Successfully added Meter No. 123456 to your account.','Insufficient Balance','ATUid_78957o68749212daso54e','2016-11-28 13:00:06'),(34,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_78957o687549y212daso54e','2016-11-28 13:08:07'),(35,114,'None','Successfully added Meter No. 123456 to your account.','Invalid Phone Number','ATUid_78957o687549y212daso54ee','2016-11-28 13:08:32'),(36,114,'None','Successfully added Meter No. 123456 to your account.','Invalid Phone Number','ATUid_78957o687549y212daso54ee','2016-11-28 13:08:55'),(37,114,'None','Successfully added Meter No. 1324321432 to your account.','Invalid Phone Number','ATUid_784957or687549y212daso54ee','2016-11-28 13:13:01'),(38,116,'None','Successfully added Meter No. 123456789 to your account.','Invalid Phone Number','ATUid_7849e5T6555r7or687549y212dasof54ee','2016-11-28 13:32:09'),(39,116,'None','Successfully added Meter No. 456789 to your account.','Invalid Phone Number','ATUid_7849e5T6TT555r7or687549y212dasof54ee','2016-11-28 13:32:44'),(40,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_7849e5T6TT555r7or687549y212dasof54ee','2016-11-28 14:11:28'),(41,117,'None','Successfully added Meter No. 234567 to your account.','Invalid Phone Number','ATUid_7849e5T6TTf555r7or687549y212dasof54ee','2016-11-28 14:11:44'),(42,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_7849e5T6TTfd555r7or687549y212dasof54ee','2016-11-29 05:08:04'),(43,118,'None','Successfully added Meter No. 123456 to your account.','Invalid Phone Number','ATUid_7849e5T6TTfd555rf7or687549y212dasof54ee','2016-11-29 05:09:18'),(44,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Insufficient Balance','ATUid_7484de6TTfd555r4ee','2016-11-29 05:13:17'),(45,119,'None','Successfully added Meter No. 12345678 to your account.','Insufficient Balance','ATUid_7484ede6TTfd555r4ee','2016-11-29 05:13:35'),(46,0,'None','Thank you Mama Baba for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_7484rede6TTfd555r4ee','2016-11-29 05:19:23'),(47,0,'None','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_7484rede64TTfd555r4ee','2016-11-29 05:23:38'),(48,121,'None','Successfully added Meter No. 123456 to your account.','Invalid Phone Number','ATUid_74e84rede64TTfd555r4ee','2016-11-29 05:24:19'),(49,122,'None','Successfully added Meter No. 345678 to your account.','Invalid Phone Number','ATUid_74e84redmde64TTfd555r4ee','2016-11-29 05:37:20'),(50,122,'None','Your Q-Power request was successful on 29-11-2016 at 8:37 AM. Token 232355879286752097 for 10 units at KES 300 for Meter No. 345678. Q-Power balance is KES 300.0','Invalid Phone Number','ATUid_743e84redmde64TTfd555r4ee','2016-11-29 05:37:46'),(51,122,'None','Your outstanding Airtime balance is KES 50.0 due on 29-12-2016.','Invalid Phone Number','ATUid_743we8r4redmde64TTfd555r4ee','2016-11-29 06:06:57'),(52,123,'None','Your Airtime request for KES 50 was successful on 29-11-2016 at 10:59 AM. Airtime balance is KES 50.0','Invalid Phone Number','ATUid_7897879wqddddrd','2016-11-29 07:59:08'),(53,123,'None','Your outstanding Airtime balance is KES 50.0 due on 29-12-2016.','Invalid Phone Number','ATUid_7897879wqddddrd','2016-11-29 07:59:38'),(54,61,'ATXid_d443968ba49b5d65b30e42969421e8a0','Your Q-Power request was successful on 01-12-2016 at 4:45 PM. Token 41433148836491337 for 10 units at KES 300 for Meter No. 12345678. Q-Power balance is KES 300.0','Success','ATUid_7897879es','2016-12-01 13:45:57'),(55,61,'ATXid_53489d61b8dc607434596608c665561b','Your Q-Power request was successful on 01-12-2016 at 4:53 PM. Token 696675988669866951 for 10 units at KES 300 for Meter No. 12345678. Q-Power balance is KES 300.0','Success','ATUid_7897879es','2016-12-01 13:53:13'),(56,61,'ATXid_b16abaf9096ef056926b492e6f4ff8f4','Your Cash request for KES 500 was successful on 01-12-2016 at 4:54 PM. Cash balance is KES 500.0','Success','ATUid_7897879es','2016-12-01 13:54:12'),(57,61,'ATXid_a7f5c5837d5010cfda6fea452548a911','Your Airtime request for KES 50 was successful on 01-12-2016 at 4:59 PM. Airtime balance is KES 50.0','Success','ATUid_7897879es','2016-12-01 13:59:44'),(58,0,'ATXid_dfd1c5d8567c832b7b5c45224a327f78','Thank you Hashim Amani for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78978479ee3434','2016-12-03 08:23:15'),(59,62,'ATXid_64d20567f3d66886d81583d21622295b','Successfully added Meter No. 111111 to your account.','Success','ATUid_78978479ee3434','2016-12-03 08:24:12'),(60,62,'ATXid_234021b73b3da94671d58f486a8bf7b5','Your Q-Power request was successful on 03-12-2016 at 11:32 AM. Token 50600926885949179 for 10 units at KES 300 for Meter No. 111111. Q-Power balance is KES 300.0','Success','ATUid_78978479oee3434','2016-12-03 08:32:03'),(61,62,'ATXid_691f5d49505c47dbe63d68a4d87e2345','You have successfully unsubscribed from Qopah services.','Success','ATUid_78978479oee3434','2016-12-03 08:42:45'),(62,0,'None','Thank you Meme Meme for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','ATUid_7897879efxsdfd','2016-12-06 05:09:53'),(63,63,'ATXid_16283b2863ef88c622a9e79ec5356d15','Your Qopah profile has been updated.','Success','ATUid_789787990dfhfgdtfidder','2016-12-06 06:21:15'),(64,64,'None','Successfully added Meter No. 33445 to your account.','Invalid Phone Number','ATUid_789787990ddfffhfgdgtfiddefr','2016-12-06 07:18:51'),(65,63,'ATXid_2e0d83822f71aef5ce986a2bdba0e5a2','Your Cash request for KES 500 was successful on 06-12-2016 at 10:43 AM. Cash balance is KES 500.0','Success','ATUid_789787f990ddgfffdhfghdgtfiddefrt','2016-12-06 07:43:22'),(66,63,'ATXid_7890938b458c0549b1ec3e1a663b3520','Your Qopah profile has been updated.','Success','ATUid_789787f990ddgfffdhfghdgtfiddefrt','2016-12-06 07:45:38'),(67,63,'ATXid_14ebec866d36319811f4adeda972c80e','Your Qopah profile has been updated.','Success','ATUid_789787f990ddgfffdhfghdgtfiddefrt','2016-12-06 07:48:24'),(68,63,'ATXid_a8fd643812a82d2a96188b9407a9fb1a','Your Qopah profile has been updated.','Success','ATUid_789787f990ddgfffdhfghDdgtfiddefrt','2016-12-06 07:50:30'),(69,0,'None','Thank you Sms Ussd for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','','2016-12-10 11:43:29'),(70,0,'ATXid_3264fe58529fb0022a05f37550687c2a','Thank you President Prezzo for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_7897879','2016-12-10 11:54:46'),(71,0,'None','Thank you Mama Mama for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Invalid Phone Number','','2016-12-10 12:17:27'),(72,0,'ATXid_bca3bf70240cb0c2a47342e8100fd46d','Thank you Wawa Wewe for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','','2016-12-10 12:20:18'),(73,151,'ATXid_abffaa7c8aac6e9abbc2bacaeca2ce3a','Successfully added Meter No. 123456 to your account.','Success','ATUid_1284182384129ssdsxddha','2016-12-14 11:26:16'),(74,151,'ATXid_d86c8f6bcb0d16dfe6527be1c2da5552','Successfully added Meter No. 123456 to your account.','Success','ATUid_128c418238d4129ssdsxddha','2016-12-14 11:39:04'),(75,151,'ATXid_1a7937b4e4a49997a095a608b721b1e5','Your Cash request for KES 500 was successful on 14-12-2016 at 2:44 PM. Cash balance is KES 500.0','Success','ATUid_128c418238d4129ssdsxddhad','2016-12-14 11:44:31'),(76,151,'ATXid_72a0972a14ddbe35e5414bd470ab89c7','Your outstanding Q-Power balance is KES 300.0 due on 21-12-2016.','Success','ATUid_128c4182c38d4129ssdsxddhad','2016-12-14 11:44:52'),(77,151,'ATXid_b2d2aa3e6f1d22121ec5c3af7605afd8','Your Airtime request for KES 50 was successful on 14-12-2016 at 2:50 PM. Airtime balance is KES 50.0','Success','ATUid_128c4182c38d412x9ssdsxddhad','2016-12-14 11:50:42'),(78,151,'ATXid_a9a3dd2ae1a99d5aabf98107b1f3eec9','Your Airtime request for KES 50 was successful on 14-12-2016 at 2:55 PM. Airtime balance is KES 50.0 Due on 21-12-2016','Success','ATUid_128c4s182c38d412x9ssdsxddhad','2016-12-14 11:55:40'),(79,151,'ATXid_0b2196081510b1ab6c8e37ebb0924e74','Your outstanding Q-Power balance is KES 300.0 due on 21-12-2016.','Success','ATUid_1287cc4s182c38ddc412xS39ssdsxddhad','2016-12-14 13:00:24'),(80,151,'ATXid_1673c86acc1e8efed30579ce202e7208','Airtime balance KES 50.0.\nCash balance KES 500.0.\nPower balance KES 300.0.\n','Success','ATUid_1287cc4s182c38ddc41c2xS39ssdsxddhad','2016-12-14 13:04:47'),(81,151,'ATXid_0fc7c5086c76a769aa2ddff6be8c95a4','Your Balances as of 14-12-2016\nAirtime balance KES 50.0.\nCash balance KES 500.0.\nPower balance KES 300.0.\n','Success','ATUid_128c7cc4s182c38dddc4c1c2xS39ssdsxddhad','2016-12-14 13:11:56'),(82,151,'ATXid_5e98447efc5670d09186b6ff2d321868','Your Outstanding Balances As Of 14-12-2016\nAirtime balance KES 50.0.\nCash balance KES 500.0.\nPower balance KES 300.0.\n','Success','ATUid_128c7ccx4s182c38dddc4c1c2xS39ssdsxddhad','2016-12-14 13:15:37'),(83,151,'ATXid_9e4a06462936e43f7d2dd5d789d6c16d','You have successfully unsubscribed from Qopah services.','Success','ATUid_12cckkkliglgxddhad','2016-12-14 16:54:40'),(84,0,'ATXid_126177f186ebcf062155ff209ab7d45d','Thank you Rama Hashim for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_12cckkkliglgxddhadk','2016-12-14 16:56:01'),(85,152,'ATXid_9eb0eabaf90b5227472d3f8646619bd0','Successfully added Meter No. 99999 to your account.','Success','ATUid_12cckkkliglgxddhadk','2016-12-14 16:56:20'),(86,152,'ATXid_4858636922da3533cac404ac58dadd73','Your Outstanding Balances As Of 14-12-2016\nYou have no outstanding balances.','Success','ATUid_12cckkkliglkgxddhadk','2016-12-14 17:14:10'),(87,152,'ATXid_cd367a5a07313dd4699b73d09f0399dd','Your Q-Power request was successful on 14-12-2016 at 9:02 PM. Token 297503275186965549 for 10 units at KES 300 for Meter No. 99999. Q-Power balance is 10.0 Units.Due on 21-12-2016','Success','ATUid_12cckkvkliglfkgxddhcadk','2016-12-14 18:02:54'),(88,152,'ATXid_30d95d5a85afbb72ec1ef29bb37c0fae','Your Q-Power request was successful on 14-12-2016 at 9:02 PM. Token 99301109096857910 for 10 units at KES 300 for Meter No. 99999. Q-Power balance is 20.0 Units.Due on 21-12-2016','Success','ATUid_12cckkvkliglfkgxddhcadk','2016-12-14 18:02:55'),(89,0,'ATXid_a84d26f83482565143c16d4413ce7a8d','Thank you Rama Hashim for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78978fdef9dc4cdk5ddsfcardddds','2016-12-15 06:37:05'),(90,153,'ATXid_01d3361ef4b0bbeefddb9948b63a71bc','Successfully added Meter No. 123456789 to your account.','Success','ATUid_78978fdef9dc4cdk5ddsfcardddds','2016-12-15 06:37:26'),(91,153,'ATXid_e1beb9ad7008fd50795c4fa8d36e3cd2','Meter Number 	1231797 updated successfully.','Success','ATUid_78978fdef9dc4cdk5ddsfcardddds','2016-12-15 06:38:52'),(92,153,'ATXid_3558dfb4e3f667112c9fa8f97e00e528','Your Q-Power request was successful on 19-12-2016 at 3:23 PM. Token 715810834761529224 for 30 units at KES 300 for Meter No. 123456789. Q-Power balance is 30.0 Units.Due on 26-12-2016','Success','ATUid_78v7j8vlpedfgkffndlfdgClscclffcpmdx','2016-12-19 12:23:29'),(93,153,'ATXid_43d1940735ef955f7b32b3447e84afaa','Your Cash request for KES 500 was successful on 19-12-2016 at 3:47 PM. Cash balance is KES 500.0.Due on 26-12-2016 ','Success','ATUid_78v7j8vlpedfgkffndfl9Clscclffcpmdx','2016-12-19 12:47:49'),(94,153,'ATXid_335c3dc0b4a91ba9664f2f20972cfa7e','Your Cash request for KES 1000 was successful on 19-12-2016 at 3:49 PM. Cash balance is KES 1000.0.Due on 26-12-2016 ','Success','ATUid_78v7j8vlpedfgkffndfl9Clscclffcpmdx','2016-12-19 12:49:13'),(95,153,'ATXid_cc676c67eae2766b3041a8d4b9ba87d9','Your Cash request for KES 2000 was successful on 19-12-2016 at 3:50 PM. Cash balance is KES 2000.0.Due on 26-12-2016 ','Success','ATUid_78v7j8vlpedfgkffndfl9Clscclffcpmdx','2016-12-19 12:50:52'),(96,153,'ATXid_38c101d1027cb20754944642fd75bca6','You have successfully unsubscribed from Qopah services.','Success','ATUid_78v7jf8vlpedfgkffndfl9Clscclffcpmdx','2016-12-19 14:03:52'),(97,156,'ATXid_3cba2c18b7999201a95374f3b81858c3','Successfully added Meter No. 543443 to your account.','Success','ATUid_78v7jf8vlpedfgkffcndfl9Clscclffcpmdx','2016-12-19 14:10:11'),(98,156,'ATXid_42ffd9ed1a87f10f0845903375b2c6dc','Your Q-Power request was successful on 27-12-2016 at 8:41 PM. Token 863155791790564636 for 10 units 300 for Meter No. 12344321. Q-Power balance is 10.0 Units.Due on 03-01-2017','Success','ATUid_78978fde9dc4dscardddsn','2016-12-27 17:41:48'),(99,156,'ATXid_d9194c549e26094931fc141f68a127ba','Your Cash request for 500 units was successful on 27-12-2016 at 8:47 PM. Cash balance is 500.0 units.Due on 03-01-2017 ','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:47:53'),(100,156,'ATXid_ae57fbe84abd3f3f9a3dd5c1d71eb6f8','Your Airtime request for 50 units was successful on 27-12-2016 at 8:53 PM. Airtime balance 50.0 units.Due on 03-01-2017','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:53:29'),(101,156,'ATXid_36214da1acf69baca060ff82a6b893f3','Your Outstanding Balances As Of 27-12-2016\nAirtime balance KES 50.0.\nCash balance KES 500.0.\nPower balance KES 10.0.\n','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:54:11'),(102,156,'ATXid_979fab146f5f256791253c0483abd287','Your Outstanding Balances As Of 27-12-2016\nAirtime balance 50.0 units.\nCash balance KES 500.0 units.\nPower balance KES 10.0 units.\n','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:55:47'),(103,156,'ATXid_39897c1d645a513cb7d1fb4f120edf0f','Your outstanding Qash balance is 500.0 units due on 03-01-2017.','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:56:10'),(104,156,'ATXid_1f65eaea999a8a4871d1c0ca5c8148e1','Your outstanding Q-Power balance is 10.0 units due on 03-01-2017.','Success','ATUid_7897f8fde9dc4dscardddsn','2016-12-27 17:56:59'),(105,156,'ATXid_28d10e857356d9ea76a3f7df426894d1','You have successfully unsubscribed from Qopah services.','Success','ATUid_78978fsde9dc4dcd5dgdscarddds','2016-12-30 17:19:25'),(106,0,'ATXid_b00a38eb05af5b05be15e8caf9d43e9e','Thank you Hashim Hash for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78978fsde9dc4dcd5dgdscardddds','2016-12-30 17:21:50'),(107,157,'ATXid_6677854b2546dc17577304e90222fb4b','Your Cash request for 500 units was successful on 30-12-2016 at 8:22 PM. Cash balance is 500.0 units.Due on 06-01-2017 ','Success','ATUid_78978fsde9dc4dcd5fdgdscardddds','2016-12-30 17:22:46'),(108,157,'ATXid_73568129cb6bec5ba0a13cc2d35ebb59','Unsubscription failed. You have outstanding debts with Qopah Fintech.','Success','ATUid_78978fsde9dc4dcd5fdgdscardddds','2016-12-30 17:23:08'),(109,157,'ATXid_c51fcf457f14ed7a82857a947ef1b302','You have successfully unsubscribed from Qopah services.','Success','ATUid_78978fsdde9dc4dcd5fdgdscardddds','2016-12-30 18:59:48'),(110,0,'ATXid_5fc2cf5662c376f784acae2ab3de91ca','Thank you 4 1 for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78978fsdde9dc4dcd5fdgdscardddds','2016-12-30 19:11:36'),(111,158,'ATXid_466e942e0ec7bbe47a3fc4a7a0ae59e3','You have successfully unsubscribed from Qopah services.','Success','ATUid_78978fsdde9dc4dcd5fdgdscardddds','2016-12-30 19:12:34'),(112,0,'ATXid_8a92ac14b01a1176f43c293f3160e616','Thank you Rama Spark for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78978fsdde9dsc4dcd5fdgdscardddds','2016-12-30 19:13:32'),(113,159,'ATXid_38ff173730277b3f4f13a6231ff27e54','Your Cash request for 500 units was successful on 30-12-2016 at 10:14 PM. Cash balance is 500.0 units.Due on 06-01-2017 ','Success','ATUid_78978fsdde9dsc4dcddgdscardddds','2016-12-30 19:14:07'),(114,159,'ATXid_01009478406cba2ab4fba35ca6a4d897','Your Airtime request for 500 units was successful on 30-12-2016 at 10:14 PM. Airtime balance 500.0 units.Due on 06-01-2017','Success','ATUid_78978fsdde9dsc4dcddgdscardddds','2016-12-30 19:15:01'),(115,159,'ATXid_8218c9932f27237f6726343a6733c210','Successfully added Meter No. 223344 to your account.','Success','ATUid_78978fsdde9dsdwfagdscardddds','2016-12-30 19:16:28'),(116,159,'ATXid_3dcf2db938955c4f3e41854c0b79650d','Your Q-Power request was successful on 30-12-2016 at 10:16 PM. Token 17757496709761640 for 10 units at 300 for Meter No. 223344. Q-Power balance is 10.0 Units.Due on 06-01-2017','Success','ATUid_78978fsdde9dsdwfagdscardddds','2016-12-30 19:16:53'),(117,162,'ATXid_3eeb26175f25ed8be33d11a03fab622a','Your For request for 20000 Kshs. was successful on 04-01-2017 at 11:04 AM. Float balance is 0.0 Kshs.Due on 11-01-2017 ','Success','ATUid_78v7jff8vfffcndfl9Clscclffcpmdx','2017-01-04 08:04:42'),(118,162,'ATXid_0133cd9911df1d739abe47b0af979b21','Your For request for 5000 Kshs. was successful on 04-01-2017 at 1:58 PM. Float balance is 0.0 Kshs.Due on 11-01-2017 ','Success','ATUid_78v7jfff8fffffcfndfflg9Clscclffgcpmdx','2017-01-04 10:58:04'),(119,0,'ATXid_03ed3712620610e6b5fc2e1b18c73fe8','Thank you Rodney Kihenjo for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_78v7jfsccflffgcpmdgxd','2017-01-04 13:13:49'),(120,162,'ATXid_032f4a04749ecbf61c7baf75aa045747','Your Float request for 5000 Kshs. was successful on 06-01-2017 at 11:43 AM. Float balance is 0.0 Kshs.Due on 13-01-2017 ','Success','ATUid_78v7ljfLsccdflffgckpgmdgxdg','2017-01-06 08:43:37'),(121,162,'ATXid_6aa77d0b16820ad6763609ce6a597944','Your Float request for 25000 Kshs. was successful on 06-01-2017 at 2:29 PM.  ','Success','ATUid_78v7ljfLsccdflffgckpgmdkfgxdg','2017-01-06 11:29:10'),(122,162,'ATXid_8b511a63687688aec6fce98c51162c2b','Your Float request for 5000 Kshs. was successful on 06-01-2017 at 4:15 PM.  ','Success','ATUid_78v7ljfLsccdflfffgcgkkpgmdkfgxdg','2017-01-06 13:15:16'),(123,162,'ATXid_c739b92cf3f9a17938e1196bd487f5a9','Your Float request for 10000 Kshs. was successful on 06-01-2017 at 4:15 PM. Float balance is 10000.0 Kshs.Due on 13-01-2017 ','Success','ATUid_78v7ljfLsccdflfffgcgkkpgmdkfgxdg','2017-01-06 13:16:00'),(124,162,'ATXid_9c823fd3f5f3975eb915a086f9ddf620','Your Float request for 5000 Kshs. was successful on 07-01-2017 at 9:45 PM.  ','Success','ATUid_78v7ljfLsccdgcgkkpgmdkfgxdg','2017-01-07 18:45:38'),(125,162,'ATXid_c151b2d64f037c254696ec39df6a8f82','Your Float request for 5000 Kshs. was successful on 09-01-2017 at 3:12 PM.  ','Success','ATUid_78978fde9dc4cd5ddscarddds','2017-01-09 12:12:23'),(126,162,'ATXid_59cefc62143071994517fe60b715f518','Your Float request for 10000 Kshs. was successful on 09-01-2017 at 3:14 PM.  ','Success','ATUid_78978fde9dc4cd5ddscarddds','2017-01-09 12:14:28'),(127,162,'ATXid_fc2b58f9d2700859e130927971ecc6ea','Your Float request for 5000 Kshs. was successful on 09-01-2017 at 3:19 PM.  ','Success','ATUid_78978fde9dc4cd5ddscarddds','2017-01-09 12:19:02'),(128,162,'ATXid_5ebd0bf6257dae8db2c1d6b312ecee1b','Your Float request for 5000 Kshs. was successful on 09-01-2017 at 3:21 PM.  ','Success','ATUid_78978fde9dc4cd5ddscarddds','2017-01-09 12:21:33'),(129,162,'ATXid_09221a31eb6178f9948b32ded35f1134','Your Float request for 5000 Kshs. was successful on 09-01-2017 at 3:34 PM. Float balance is 5000.0 Kshs.Due on 16-01-2017 ','Success','ATUid_78978fde9dc4cd5ddscarddds','2017-01-09 12:34:17'),(130,162,'ATXid_5bb68fa113bb5d6a81696f7e9a4775db','Your Float request for 25000 Kshs. was successful on 09-01-2017 at 3:39 PM.  ','Success','ATUid_78978fde9dfc4cd5ddscarddds','2017-01-09 12:39:07'),(131,162,'ATXid_67f2b9e956f9f036bbe043bb801479ed','Your outstanding Qash balance is 100.0 units due on 16-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscarddds','2017-01-09 13:15:06'),(132,162,'ATXid_36878fd573b25f4e78fec4548bce4e1c','Your outstanding Qash balance is 200.0 units due on 16-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscarddds','2017-01-09 13:16:41'),(133,162,'ATXid_97cdc29796f97f9d55751d3e64ec30d4','Your outstanding Qash balance is 100.0 units due on 16-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscarddds','2017-01-09 13:17:25'),(134,162,'ATXid_408d65f888e4844403e0542e0df557d1','Your Cash request for 500 units was successful on 09-01-2017 at 7:19 PM. Cash balance is 0.0 units.Due on 16-01-2017 ','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 16:19:51'),(135,162,'ATXid_8af76dfd2782075ab2a1cb8dee1ae38f','Your outstanding Qash balance is 500.0 units due on 16-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 16:22:28'),(136,162,'ATXid_4f5fe6c4fd45048fc87bb7d0cc8f4f65','Your outstanding Qash balance is 500.0 units due on 16-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 17:05:01'),(137,162,'ATXid_7172eaf00594c21444e69f23fdc97202','Your Outstanding Balances As Of 09-01-2017\nYou have no outstanding balances.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 17:34:51'),(138,162,'ATXid_2e83c68c82a521ea04091a5e598f856f','Your Outstanding Balances As Of 09-01-2017\nYou have no outstanding balances.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 17:40:52'),(139,162,'ATXid_eb8e88dd5f43c58382c44184e302a5f6','Your Outstanding Balances As Of 09-01-2017\nYou have no outstanding balances.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 17:42:56'),(140,162,'ATXid_9c49dc612d9da9b4f4bbc63b06ad112e','Your Outstanding Balances As Of 09-01-2017\nFloat balance KES 5450.0 units.\n','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 17:46:18'),(141,162,'ATXid_b375365f3ef7b560a0797bb2d3e71c5b','Your Float repayment for 4000 units was successful on 09-01-2017 at 9:07 PM. Float balance is 5450.0 units.  ','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 18:07:57'),(142,162,'ATXid_06f589905b5d9514e10ee7ebabb57817','Your Float repayment for 4000 units was successful on 09-01-2017 at 9:10 PM. Float balance is 5450.0 units.  ','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 18:10:10'),(143,162,'ATXid_f52d91294cfe8f62165b429bfd483a14','Your Float repayment for 1000 units was successful on 09-01-2017 at 9:12 PM. Float balance is 450.0 units.  ','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-09 18:12:07'),(144,162,'ATXid_011fa8b7e29aad6a1f3f50c748eb45a8','Your Q-Power request was successful on 10-01-2017 at 8:06 AM. Token 734160516192289582 for 10 units at 300 for Meter No. 123456. Q-Power balance is -480.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-10 05:06:34'),(145,162,'ATXid_626adb1a276e6dd8a1060e0aa7f7a1bd','Your outstanding Q-Power balance is 20.0 units due on 17-01-2017.','Success','ATUid_78978fde9dfc4cd5ddscardddsd','2017-01-10 05:16:50'),(146,162,'ATXid_fce5c990ce2187077e3728e49a1a55d8','Your outstanding Kopa Float balance is 450.0 Kshs. due on 17-01-2017.','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 05:35:01'),(147,162,'ATXid_ccd42a68518e3b7f7351783a73595193','Your Q-Power request was successful on 10-01-2017 at 9:53 AM. Token 41620653423269485 for 20 units at 300 for Meter No. 123456. Q-Power balance is 30.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 06:53:26'),(148,162,'ATXid_f44f9dfae5ba440f39349f7b876f9f5c','Your Q-Power request was successful on 10-01-2017 at 10:08 AM. Token 743986145533421419 for 40 units at 300 for Meter No. 123456. Q-Power balance is 70.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 07:08:01'),(149,162,'ATXid_974cd2b77d8cccf3589ef7c75dcc6342','Your Q-Power request was successful on 10-01-2017 at 10:10 AM. Token 9382020096457329 for 40 units at 300 for Meter No. 123456. Q-Power balance is 110.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 07:10:13'),(150,162,'ATXid_018b7cd3cb3933004d0b3afe7142e1f3','Your Q-Power request was successful on 10-01-2017 at 10:15 AM. Token 62173310812851045 for 40 units at 300 for Meter No. 123456. Q-Power balance is 150.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 07:15:23'),(151,162,'ATXid_e92bff5c25d2d5fea13c83014a971d5d','Your Q-Power request was successful on 10-01-2017 at 11:10 AM. Token 45236295326884324 for 40 units at 300 for Meter No. 123456. Q-Power balance is 190.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:10:30'),(152,162,'ATXid_785a01215a175ebb0352f1ad8ec8a895','Your Q-Power request was successful on 10-01-2017 at 11:25 AM. Token 889345557823738253 for 40 units at 300 for Meter No. 123456. Q-Power loan balance is 90.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:25:38'),(153,162,'ATXid_18ae319acfac9b5e5cd47761ee760e96','Your Q-Power request was successful on 10-01-2017 at 11:27 AM. Token 374399184826337297 for 40 units at 300 for Meter No. 123456. Q-Power loan balance is 50.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:27:03'),(154,162,'ATXid_c8405c56204a664e594f4ac0b54d9e58','Your Q-Power request was successful on 10-01-2017 at 11:29 AM. Token 366106929421868717 for 40 units at 300 for Meter No. 123456. Q-Power loan balance is 10.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:29:01'),(155,162,'ATXid_61e19133fd2fbbb4bda85f65d768289b','Your Q-Power request was successful on 10-01-2017 at 11:29 AM. Token 564665205001857433 for 40 units at 300 for Meter No. 123456. Q-Power loan balance is -30.0 Units.','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:29:29'),(156,162,'ATXid_2be32f77615a2f7e84b5e38acc7bdea7','Your Q-Power request was successful on 10-01-2017 at 11:38 AM. Token 395818193157183118 for 10 units at 300 for Meter No. 123456.','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:38:13'),(157,162,'ATXid_40526127d3345c2293018016a8bcdf34','Your Q-Power request was successful on 10-01-2017 at 11:39 AM. Token 43660230079123522 for 20 units at 300 for Meter No. 123456. Q-Power balance is 20.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:39:16'),(158,162,'ATXid_e883f92e23f7b6dace2bc05f2bbc9f9f','Your Q-Power request was successful on 10-01-2017 at 11:39 AM. Token 528599222981475146 for 20 units at 300 for Meter No. 123456.','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:39:47'),(159,162,'ATXid_2d6a834e7fc2a355d14d07f497af2f6e','Your Q-Power request was successful on 10-01-2017 at 11:41 AM. Token 287798519505516468 for 10 units at 300 for Meter No. 123456. Q-Power balance is 10.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:41:16'),(160,162,'ATXid_b24706207892cc4fdfefc3e53623b4bd','Your Q-Power request was successful on 10-01-2017 at 11:41 AM. Token 489163164767734200 for 10 units at 300 for Meter No. 123456. 10 units have been deducted to repay your loan. Q-Power outstanding loan balance is 0.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 08:41:37'),(161,162,'ATXid_66c3c44cf5125daed46bc1e000780ce3','Your Airtime request for 50 units was successful on 10-01-2017 at 2:16 PM. Airtime balance 50.0 units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscardddsd','2017-01-10 11:16:05'),(162,162,'ATXid_2c38486db136bd17732e6485cbbfe2b7','Your Airtime request for 50 units was successful on 10-01-2017 at 3:20 PM. 50 was deducted to repay your loan. Airtime balance 0.0 units.','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 12:20:04'),(163,162,'ATXid_0ccb4b20622d2217d3baddbb24420844','Your Airtime request for 250 units was successful on 10-01-2017 at 3:22 PM. Airtime balance 250.0 units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 12:22:02'),(164,162,'ATXid_f09b05b3b4b122b16dc8168324a542ba','Your Airtime request for 100 units was successful on 10-01-2017 at 3:23 PM. 100 was deducted to repay your loan. Airtime balance 150.0 units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 12:23:51'),(165,162,'ATXid_2cd2f4cc7660f985c9e532ae5288face','Your Airtime request for 50 units was successful on 10-01-2017 at 4:08 PM. 50 was deducted to repay your loan. Airtime balance 100.0 units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:08:20'),(166,162,'ATXid_6a1887993fd558f9cf6142d3eb533b18','Your Airtime request for 100 units was successful on 10-01-2017 at 4:10 PM. 100 was deducted to repay your loan. Airtime balance 0.0 units.','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:10:42'),(167,162,'ATXid_18355c642b8e25a07f69d55891246f0f','Your Q-Power request was successful on 10-01-2017 at 4:20 PM. Token 349941140223101913 for 40 units at 300 for Meter No. 123456. Q-Power balance is 40.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:20:08'),(168,162,'ATXid_ed2f2c3335eefadf1aad1494df8b1a2c','Your Q-Power request was successful on 10-01-2017 at 4:20 PM. Token 391974505534147126 for 10 units at 300 for Meter No. 123456. 10 units have been deducted to repay your loan. Q-Power outstanding loan balance is 30.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:20:59'),(169,162,'ATXid_e49dc8cc0af636396025af47c983df46','Your Q-Power request was successful on 10-01-2017 at 4:24 PM. Token 33189987858097902 for 20 units at 300 for Meter No. 123456. 20 units have been deducted to repay your loan. Q-Power outstanding loan balance is 10.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:24:28'),(170,162,'ATXid_318ef09d5d380c00ff58c99b2c51427e','Your Q-Power request was successful on 10-01-2017 at 4:37 PM. Token 369592132153963166 for 40 units at 300 for Meter No. 123456.','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 13:37:22'),(171,162,'ATXid_f15f1b6fe39a569d49ada2dd142ab6b6','Your Q-Power request was successful on 10-01-2017 at 5:03 PM. Token 219408993538149431 for 20 units at 300 for Meter No. 123456. Q-Power balance is 0.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 14:03:52'),(172,162,'ATXid_eddb9144362eaca61dcf9ee12f52de8f','Your Q-Power request was successful on 10-01-2017 at 5:06 PM. Token 619149579486235595 for 10 units at 300 for Meter No. 123456. Q-Power balance is 10.0 Units.Due on 17-01-2017','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 14:06:41'),(173,162,'ATXid_d7e74595372398f87d372497f8eab860','Your Q-Power request was successful on 10-01-2017 at 5:16 PM. Token 8758272854611843 for 20 units at 300 for Meter No. 123456.','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-10 14:16:51'),(174,162,'ATXid_9be0ee900229dd8bc8402f690061b704','Your Cash repayment for 400 Kshs. was successful on 11-01-2017 at 10:46 AM. Cash Loan balance is -400.0 units.  ','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-11 07:46:55'),(175,162,'ATXid_58075bbc5288d9a6fc4c3b32c81c08d2','Your Cash repayment for 70 Kshs. was successful on 11-01-2017 at 10:50 AM. Cash Loan balance is -470.0 units.  ','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-11 07:50:54'),(176,162,'ATXid_9d7b3769ab2851d38fb7ddee61900a95','Your Cash request for 1000 units was successful on 11-01-2017 at 10:57 AM. Cash balance is 530.0 units.Due on 18-01-2017 ','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-11 07:58:03'),(177,162,'ATXid_a36f9122c0e66533d15ffde0bb42dcb5','Your Cash repayment for 530 Kshs. was successful on 11-01-2017 at 10:58 AM. Cash Loan balance is 0.0 units.  ','Success','ATUid_78978fde9dfc4cdf5ddscfardddsd','2017-01-11 07:58:36'),(178,162,'ATXid_79650624138e336ab6af15e617b0b434','Your Cash request for 5000 units was successful on 11-01-2017 at 11:26 AM. Cash balance is 5000.0 units.Due on 18-01-2017 ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 08:27:04'),(179,162,'ATXid_849f84ec968fe5f6f858aa3d5f208d7a','Your Airtime request for 500 units was successful on 11-01-2017 at 11:27 AM. Airtime balance 500.0 units.Due on 18-01-2017','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 08:27:37'),(180,162,'ATXid_4fc7075791e91d17edf7c001f97d94ce','Your Float request for 20000 Kshs. was successful on 11-01-2017 at 11:29 AM.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 08:29:31'),(181,162,'ATXid_8461525c7811d5dc11a7177ce3104b42','Your outstanding Qash balance is 5000.0 units due on 18-01-2017.','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 08:29:46'),(182,162,'ATXid_1f0fc24c6cbb8df905771b382ee63247','Your Cash repayment for -90 Kshs. was successful on 11-01-2017 at 12:19 PM. Cash Loan balance is -2510.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 09:19:53'),(183,162,'ATXid_deea104d6340368d2e7ffa305a55c53f','Your Cash repayment for -777 Kshs. was successful on 11-01-2017 at 12:23 PM. Cash Loan balance is -1733.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 09:24:08'),(184,162,'ATXid_3f5a9470026019073afe2b47ae1a5e28','Your Cash repayment for -987 Kshs. was successful on 11-01-2017 at 12:27 PM. Cash Loan balance is -746.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 09:28:32'),(185,162,'ATXid_f129d2ceb3ade9d590233e5d0a3283d8','Your Cash repayment for 0 Kshs. was successful on 11-01-2017 at 12:29 PM. Cash Loan balance is -746.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 09:29:57'),(186,162,'ATXid_842869910c306fcc4b0f697238113b07','Your Cash repayment for 9000 Kshs. was successful on 11-01-2017 at 12:30 PM. Cash Loan balance is -9746.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-11 09:30:33'),(187,162,'ATXid_7ea8f10cacfeb4864d99d8c383eede95','Your Float repayment for 0 units was successful on 13-01-2017 at 8:48 PM. Float Loan balance is 0.0 units.  ','Success','ATUid_78978fde9dfc4cdf5Fddscfardddsd','2017-01-13 17:48:20'),(188,162,'ATXid_ad5d1123298e8c5198f479f2d44c0a9d','Your Airtime request for 50 units was successful on 13-01-2017 at 8:51 PM. Airtime balance 50.0 units.Due on 20-01-2017','Success','ATUid_78978fde9dffc4cdf5Fddscfardddsd','2017-01-13 17:51:03'),(189,162,'ATXid_05d6ee03398b3a95d7b976be4af0f52f','Your Airtime request for 10 units was successful on 13-01-2017 at 9:41 PM. Airtime balance 10.0 units.Due on 20-01-2017','Success','ATUid_78978fde9dffc4cdf5Fddscfardddsd','2017-01-13 18:41:59'),(190,162,'ATXid_e4b05f9a64e47a55f802d56d39699a4c','Your Airtime request for 10 units was successful on 13-01-2017 at 9:52 PM. Airtime balance 10.0 units.Due on 20-01-2017','Success','ATUid_78978fde9dffc4cdf5Fddscfardddsd','2017-01-13 18:52:43'),(191,162,'ATXid_f235bab37ea861d26b929695c1f2a0ca','Your Q-Power request was successful on 02-02-2017 at 8:01 AM. Token 292429133284529456 for 20 units at 300 for Meter No. 123456.','Success','ATUid_165e8bc61a92afjafarted2a1ad3c7a72eabl','2017-02-02 05:01:37'),(192,162,'ATXid_6b47648e7d6db2f77d69ef381d3b24c3','Your Q-Power request was successful on 02-02-2017 at 8:31 AM. Token 514118999326766656 for 40 units at 300 for Meter No. 123456.','Success','ATUid_165e8bc61a92afjafarted2a1ad3c7a72eabl','2017-02-02 05:31:44'),(193,162,'ATXid_86041c91c962ec6293a0f94659bb65cc','Your Q-Power request was successful on 02-02-2017 at 8:47 AM. Token 799955280963462694 for 20 units at 300 for Meter No. 123456.','Success','ATUid_165e8bc61a92afjafarted2a1ad3c7a72eabll','2017-02-02 05:47:06'),(194,0,'ATXid_d889992cfc1e2e91a2dc23b2f7b14577','Thank you Admin Admin for registering with Qopah. Please keep your PIN safely. Dial *483*789# to access Qopah services.','Success','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 08:41:13'),(195,176,'ATXid_ac3c310686caf0e5d432882e18abdce8','You have registered your business with Qopah Services./n Business name:Qopah/n Certificate number:rt67543/n Physical Address:KAren/n Box number:2345/n Your details are being verified. We will get back to you once its done./n Thankyou for using Qopah Services.','Success','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 09:34:43'),(196,176,'ATXid_ad153ea5909b3218f12d4cd41be596a5','You have registered your business with Qopah Services.\n Business name:Qopah\n Certificate number:rt56789\n Physical Address:Rongai\n Box number:342\n Your details are being verified. We will get back to you once its done.\n Thankyou for using Qopah Services.','Success','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 09:37:35'),(197,176,'ATXid_c04fa7d6932aaa286e7638ee3bcfae74','You have registered your business with Qopah Services.\n1. Business name:Qopah Services\n2. Certificate number:rt5463\n3. Physical Address:Karen\n4. Box number:342\nYour details are being verified. We will get back to you once its done.\n Thankyou for using Qopah Services.','Success','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 09:46:01'),(198,176,'ATXid_ef2b4b9fa0b3428eab384b12d6ee186c','You have requested a loan from Qopah services.\n1. Amount:4\n2. Period:2\n3. Currency:dollars\n4. Delivery Channel:cheque\nYour details are being verified. We will get back to you once its done.\nThankyou for using Qopah Services.','Success','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 13:31:14'),(199,176,'ATXid_7f4bdc213ac789a94a881fde987aabc6','You have requested a loan from Qopah services.\n1. Amount:90000\n2. Period:4\n3. Currency:kes\n4. Delivery Channel:bank_account\nYour details are being verified. We will get back to you once its done.\nThankyou for using Qopah Services.','Success','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 11:00:46');
/*!40000 ALTER TABLE `qp_outbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_permission_groups`
--

DROP TABLE IF EXISTS `qp_permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_permission_groups` (
  `groupID` int(11) NOT NULL AUTO_INCREMENT,
  `groupName` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_permission_groups`
--

LOCK TABLES `qp_permission_groups` WRITE;
/*!40000 ALTER TABLE `qp_permission_groups` DISABLE KEYS */;
INSERT INTO `qp_permission_groups` VALUES (1,'superadmin'),(2,'administrator'),(3,'users'),(4,'auditors');
/*!40000 ALTER TABLE `qp_permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_permission_map`
--

DROP TABLE IF EXISTS `qp_permission_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_permission_map` (
  `groupID` int(11) NOT NULL DEFAULT '0',
  `permissionID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`groupID`,`permissionID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_permission_map`
--

LOCK TABLES `qp_permission_map` WRITE;
/*!40000 ALTER TABLE `qp_permission_map` DISABLE KEYS */;
INSERT INTO `qp_permission_map` VALUES (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24);
/*!40000 ALTER TABLE `qp_permission_map` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_permissions`
--

DROP TABLE IF EXISTS `qp_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_permissions` (
  `permissionID` int(11) NOT NULL AUTO_INCREMENT,
  `permission` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `key` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`permissionID`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_permissions`
--

LOCK TABLES `qp_permissions` WRITE;
/*!40000 ALTER TABLE `qp_permissions` DISABLE KEYS */;
INSERT INTO `qp_permissions` VALUES (1,'read_view_clients','read_view_clients','clients'),(2,'create_clients','create_clients','clients'),(3,'update_clients','update_clients','clients'),(4,'delete_clients','delete_clients','clients'),(5,'read_view_electricity_tokens','read_view_electricity_tokens','electricity_tokens'),(6,'create_electricity_tokens','create_electricity_tokens','electricity_tokens'),(7,'update_electricity_tokens','update_electricity_tokens','electricity_tokens'),(8,'delete_electricity_tokens','delete_electricity_tokens','electricity_tokens'),(9,'read_view_archived_tokens','read_view_archived_tokens','archived_tokens'),(10,'create_archived_tokens','create_archived_tokens','archived_tokens'),(11,'update_archived_tokens','update_archived_tokens','archived_tokens'),(12,'delete_archived_tokens','delete_archived_tokens','archived_tokens'),(13,'read_view_transactions','read_view_transactions','transactions'),(14,'create_transactions','create_transactions','transactions'),(15,'update_transactions','update_transactions','transactions'),(16,'delete_transactions','delete_transactions','transactions'),(17,'read_view_reports','read_view_reports','reports'),(18,'create_reports','create_reports','reports'),(19,'update_reports','update_reports','reports'),(20,'delete_reports','delete_reports','reports'),(21,'read_view_system_users','read_view_system_users','system_users'),(22,'create_system_users','create_system_users','system_users'),(23,'update_system_users','update_system_users','system_users'),(24,'delete_system_users','delete_system_users','system_users');
/*!40000 ALTER TABLE `qp_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_sms_activity`
--

DROP TABLE IF EXISTS `qp_sms_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_sms_activity` (
  `activity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_number` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `description` int(11) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_sms_activity`
--

LOCK TABLES `qp_sms_activity` WRITE;
/*!40000 ALTER TABLE `qp_sms_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_sms_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_system_user_status`
--

DROP TABLE IF EXISTS `qp_system_user_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_system_user_status` (
  `status_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `status_name` varchar(255) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_system_user_status`
--

LOCK TABLES `qp_system_user_status` WRITE;
/*!40000 ALTER TABLE `qp_system_user_status` DISABLE KEYS */;
INSERT INTO `qp_system_user_status` VALUES (1,'Inactive'),(2,'Active'),(3,'Suspended'),(4,'Deleted');
/*!40000 ALTER TABLE `qp_system_user_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_system_users`
--

DROP TABLE IF EXISTS `qp_system_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_system_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `country_id` int(11) unsigned NOT NULL,
  `postal_address` text NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `city_id` int(11) unsigned NOT NULL,
  `account_status` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_system_users`
--

LOCK TABLES `qp_system_users` WRITE;
/*!40000 ALTER TABLE `qp_system_users` DISABLE KEYS */;
INSERT INTO `qp_system_users` VALUES (1,'Alex','Bengo','info@ebitsonline.com','0721985408',101,'19436','00100',5,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','',NULL,'',NULL,'',NULL,NULL,NULL,NULL,0,NULL,NULL),(2,'Rodney','Kihenjo','rodney.kihenjo@gmail.com','0722765724',1,'1','1',1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','',NULL,'',NULL,'',NULL,NULL,NULL,NULL,0,NULL,NULL),(3,'Frank','Mwangi','phrank.mwangi@gmail.com','0721',1,'1','1',1,2,'2016-03-09 00:00:00','2016-03-21 10:59:36','',NULL,'',NULL,'',NULL,NULL,NULL,NULL,0,NULL,NULL),(4,'Michael','Kabugi','michael.kabugi@gmail.com','0722',1,'1','1',1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','',NULL,'',NULL,'',NULL,NULL,NULL,NULL,0,NULL,NULL);
/*!40000 ALTER TABLE `qp_system_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_token_status`
--

DROP TABLE IF EXISTS `qp_token_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_token_status` (
  `status_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `status_name` varchar(255) NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_token_status`
--

LOCK TABLES `qp_token_status` WRITE;
/*!40000 ALTER TABLE `qp_token_status` DISABLE KEYS */;
INSERT INTO `qp_token_status` VALUES (1,'Acquired'),(2,'Sent'),(3,'Disputed'),(4,'Investigated'),(5,'Invalid');
/*!40000 ALTER TABLE `qp_token_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_tokens`
--

DROP TABLE IF EXISTS `qp_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_tokens` (
  `token_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `token_name` varchar(255) NOT NULL,
  `token_worth` float NOT NULL,
  `token_units` float NOT NULL,
  `token_serial_number` varchar(255) NOT NULL,
  `expiry_date` date NOT NULL,
  `token_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_tokens`
--

LOCK TABLES `qp_tokens` WRITE;
/*!40000 ALTER TABLE `qp_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_transaction_types`
--

DROP TABLE IF EXISTS `qp_transaction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_transaction_types` (
  `transaction_type_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `transaction_type` varchar(100) NOT NULL,
  PRIMARY KEY (`transaction_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_transaction_types`
--

LOCK TABLES `qp_transaction_types` WRITE;
/*!40000 ALTER TABLE `qp_transaction_types` DISABLE KEYS */;
INSERT INTO `qp_transaction_types` VALUES (1,'USSD'),(2,'SMS');
/*!40000 ALTER TABLE `qp_transaction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_transactions`
--

DROP TABLE IF EXISTS `qp_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_transactions` (
  `transaction_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `mobile_number` varchar(255) NOT NULL,
  `meter_number` varchar(255) NOT NULL,
  `token_id` int(11) unsigned NOT NULL,
  `transaction_type` int(11) unsigned NOT NULL,
  `transaction_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_transactions`
--

LOCK TABLES `qp_transactions` WRITE;
/*!40000 ALTER TABLE `qp_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_user_access`
--

DROP TABLE IF EXISTS `qp_user_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_user_access` (
  `access_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `user_id` int(11) unsigned NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `two_factor_permission` tinyint(1) NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `account_status` int(11) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `last_access` datetime NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  PRIMARY KEY (`access_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_user_access`
--

LOCK TABLES `qp_user_access` WRITE;
/*!40000 ALTER TABLE `qp_user_access` DISABLE KEYS */;
INSERT INTO `qp_user_access` VALUES (1,1,'info@ebitsonline.com','$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a',1,1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','2016-03-09 00:00:00',''),(2,2,'rodney.kihenjo@gmail.com','$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a',1,1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','2016-03-09 00:00:00',''),(3,3,'phrank.mwangi@gmail.com','$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a',1,1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','2016-03-09 00:00:00',''),(4,4,'michael.kabugi@gmail.com','$2y$10$.vGA1O9wmRjrwAVXD98HNOgsNpDczlqm3Jq7KnEd1rVAGv3Fykk1a',1,1,1,'2016-03-09 00:00:00','2016-03-09 00:00:00','2016-03-09 00:00:00','');
/*!40000 ALTER TABLE `qp_user_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_user_activity`
--

DROP TABLE IF EXISTS `qp_user_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_user_activity` (
  `activity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `user_id` int(10) unsigned NOT NULL,
  `activity_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `system_description` text NOT NULL,
  `user_remarks` longtext NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_user_activity`
--

LOCK TABLES `qp_user_activity` WRITE;
/*!40000 ALTER TABLE `qp_user_activity` DISABLE KEYS */;
/*!40000 ALTER TABLE `qp_user_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_users`
--

DROP TABLE IF EXISTS `qp_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `postal_address` text,
  `postal_code` varchar(100) NOT NULL,
  `physical_address` text NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_users`
--

LOCK TABLES `qp_users` WRITE;
/*!40000 ALTER TABLE `qp_users` DISABLE KEYS */;
INSERT INTO `qp_users` VALUES (1,1,'127.0.0.1','rodney','$2y$08$1sPP4U8VFPkDw4shEZN6be/mKbbrbD83blF1FYG6nFmcH3PgdTnUe','','admin@qopah.com','',NULL,NULL,NULL,1268889823,1463132754,1,'Rodney','Kihenjo','1245 Nairobi','00100','Langata Estate','0722765724'),(2,1,'127.0.0.1','mrbengo','$2y$08$iXYxfo5TULReqrBClIp2g.4Jdc.V6JDAY7/eK4rIfE3A6ecTqdDNG',NULL,'info@ebitsonline.com',NULL,NULL,NULL,NULL,1460515154,1465821340,1,'Alex','Bengo','19436 Nairobi','00100','South B','0721985408'),(8,2,'197.237.111.8','frank','$2y$08$JdFAlCgc1SCv5o2HzOXfgOxSTwMnjupwrIGKzlJsOhi.MV9fqQ/9K',NULL,'frank@qopah.com',NULL,NULL,NULL,NULL,1461309001,1466070956,1,'Frank','Njenga','15428 Nairobi','00100','Karen','0722765724');
/*!40000 ALTER TABLE `qp_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_users_groups`
--

DROP TABLE IF EXISTS `qp_users_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `qp_groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `qp_users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_users_groups`
--

LOCK TABLES `qp_users_groups` WRITE;
/*!40000 ALTER TABLE `qp_users_groups` DISABLE KEYS */;
INSERT INTO `qp_users_groups` VALUES (9,1,1),(3,2,1),(16,8,1);
/*!40000 ALTER TABLE `qp_users_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_ussd_activity`
--

DROP TABLE IF EXISTS `qp_ussd_activity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_ussd_activity` (
  `activity_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_number` varchar(255) NOT NULL,
  `session_id` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL,
  `description` int(11) NOT NULL,
  PRIMARY KEY (`activity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_ussd_activity`
--

LOCK TABLES `qp_ussd_activity` WRITE;
/*!40000 ALTER TABLE `qp_ussd_activity` DISABLE KEYS */;
INSERT INTO `qp_ussd_activity` VALUES (1,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:40:20',20),(2,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:40:26',20),(3,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:40:40',20),(4,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:40:51',20),(5,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:40:56',20),(6,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:41:05',20),(7,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:41:12',20),(8,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7a72eab','2017-02-28 11:41:25',20),(9,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-02-28 11:41:32',20),(10,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-02-28 11:41:37',20),(11,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-03-01 15:51:59',20),(12,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-03-01 15:52:43',20),(13,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-03-01 15:53:16',20),(14,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-03-01 16:01:01',20),(15,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','2017-03-01 16:01:07',20),(16,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:07:37',20),(17,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:07:45',20),(18,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:07:52',20),(19,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:08:50',20),(20,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:08:56',20),(21,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:09:02',20),(22,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:11:06',20),(23,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:11:11',20),(24,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:11:43',20),(25,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:11:48',20),(26,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:13:46',20),(27,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:19:36',20),(28,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 13:20:03',20),(29,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:28:01',20),(30,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:28:08',20),(31,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:30:18',20),(32,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:30:23',20),(33,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:33:22',20),(34,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 15:33:27',20),(35,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 16:37:19',20),(36,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 16:37:34',20),(37,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-02 16:37:38',20),(38,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 07:56:04',20),(39,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 07:56:11',20),(40,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 07:56:38',20),(41,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:11:18',20),(42,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:11:29',20),(43,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:11:33',20),(44,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:11:38',20),(45,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:27:29',20),(46,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:27:34',20),(47,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:27:41',20),(48,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 08:28:01',20),(49,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:51:59',20),(50,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:52:04',20),(51,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:52:08',20),(52,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:52:18',20),(53,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:56:15',20),(54,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:56:20',20),(55,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:56:24',20),(56,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:56:42',20),(57,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 09:57:02',20),(58,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:14',20),(59,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:17',20),(60,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:21',20),(61,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:31',20),(62,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:37',20),(63,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:44',20),(64,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:10:51',20),(65,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:22',20),(66,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:26',20),(67,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:32',20),(68,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:38',20),(69,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:44',20),(70,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:14:51',20),(71,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:15:11',20),(72,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:18:07',20),(73,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:18:11',20),(74,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:18:15',20),(75,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:18:23',20),(76,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:19:56',20),(77,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:22:48',20),(78,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:25:15',20),(79,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','2017-03-06 10:28:38',20),(80,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:29:19',20),(81,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:29:30',20),(82,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:29:36',20),(83,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:29:43',20),(84,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:29:52',20),(85,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:30:03',20),(86,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:30:11',20),(87,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:30:18',20),(88,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:30:28',20),(89,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:37:35',20),(90,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:37:42',20),(91,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:37:45',20),(92,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:37:56',20),(93,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:38:01',20),(94,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:38:07',20),(95,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:38:50',20),(96,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:38:56',20),(97,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:22',20),(98,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:26',20),(99,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:30',20),(100,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:37',20),(101,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:43',20),(102,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:49',20),(103,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:39:53',20),(104,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:40:10',20),(105,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:40:45',20),(106,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:40:48',20),(107,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:40:52',20),(108,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:40:59',20),(109,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:41:03',20),(110,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:41:14',20),(111,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:41:19',20),(112,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 10:41:23',20),(113,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:00:38',20),(114,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:00:46',20),(115,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:00:55',20),(116,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:01:02',20),(117,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:01:09',20),(118,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:02:44',20),(119,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:03:54',20),(120,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:19',20),(121,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:23',20),(122,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:29',20),(123,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:34',20),(124,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:43',20),(125,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:51',20),(126,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:04:56',20),(127,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:26',20),(128,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:31',20),(129,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:36',20),(130,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:41',20),(131,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:48',20),(132,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:12:54',20),(133,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:13:01',20),(134,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:13:15',20),(135,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:21',20),(136,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:25',20),(137,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:30',20),(138,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:36',20),(139,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:42',20),(140,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:51',20),(141,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:16:56',20),(142,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:17:04',20),(143,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:33:58',20),(144,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:02',20),(145,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:06',20),(146,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:13',20),(147,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:21',20),(148,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:26',20),(149,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:31',20),(150,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:34:46',20),(151,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:11',20),(152,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:15',20),(153,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:22',20),(154,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:33',20),(155,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:40',20),(156,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:46',20),(157,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:35:51',20),(158,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:37:04',20),(159,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:41:20',20),(160,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:41:25',20),(161,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:53:15',20),(162,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:53:19',20),(163,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:56:48',20),(164,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 11:56:53',20),(165,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:00:33',20),(166,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:00:37',20),(167,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:01:18',20),(168,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:01:23',20),(169,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:01:28',20),(170,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:03:44',20),(171,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:03:48',20),(172,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:07:35',20),(173,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:07:39',20),(174,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:16:07',20),(175,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','2017-03-06 12:17:08',20),(176,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:19:28',20),(177,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:21:56',20),(178,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:01',20),(179,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:06',20),(180,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:11',20),(181,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:18',20),(182,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:23',20),(183,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:30',20),(184,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:34',20),(185,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:22:40',20),(186,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:24:41',20),(187,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:24:46',20),(188,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:24:49',20),(189,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:24:55',20),(190,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:25:00',20),(191,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:25:06',20),(192,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:25:10',20),(193,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:25:16',20),(194,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:27:12',20),(195,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:33:59',20),(196,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:04',20),(197,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:07',20),(198,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:15',20),(199,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:21',20),(200,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:27',20),(201,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:33',20),(202,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:34:41',20),(203,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:36:12',20),(204,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:36:15',20),(205,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:36:22',20),(206,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:36:49',20),(207,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:36:54',20),(208,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:37:09',20),(209,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:37:14',20),(210,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:37:33',20),(211,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:40:24',20),(212,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:40:29',20),(213,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:40:36',20),(214,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:40:49',20),(215,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:40:55',20),(216,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:41:02',20),(217,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:41:07',20),(218,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:41:14',20),(219,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:01',20),(220,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:04',20),(221,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:20',20),(222,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:30',20),(223,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:37',20),(224,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:42',20),(225,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:45:47',20),(226,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-06 12:46:00',20),(227,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:07:50',20),(228,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:07:57',20),(229,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:09:21',20),(230,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:09:25',20),(231,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:09:31',20),(232,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:11:32',20),(233,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:11:39',20),(234,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:11:43',20),(235,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:18:25',20),(236,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:18:30',20),(237,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:18:34',20),(238,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:37:57',20),(239,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:44:41',20),(240,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:44:46',20),(241,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:44:52',20),(242,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 09:45:00',20),(243,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:19:56',20),(244,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:01',20),(245,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:05',20),(246,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:18',20),(247,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:25',20),(248,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:35',20),(249,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:20:56',20),(250,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:41:52',20),(251,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:41:57',20),(252,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:42:03',20),(253,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:42:11',20),(254,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:42:18',20),(255,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:42:53',20),(256,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:42:58',20),(257,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:59:11',20),(258,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:59:45',20),(259,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:59:49',20),(260,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:59:52',20),(261,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 10:59:56',20),(262,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:01:01',20),(263,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:03:29',20),(264,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:05:16',20),(265,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:13:59',20),(266,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:04',20),(267,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:11',20),(268,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:16',20),(269,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:23',20),(270,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:27',20),(271,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 11:14:35',20),(272,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:05',20),(273,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:21',20),(274,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:26',20),(275,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:33',20),(276,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:43',20),(277,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:00:52',20),(278,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 15:01:00',20),(279,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:05',20),(280,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:09',20),(281,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:12',20),(282,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:17',20),(283,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:22',20),(284,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:27',20),(285,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:13:42',20),(286,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:24',20),(287,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:27',20),(288,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:31',20),(289,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:40',20),(290,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:49',20),(291,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:53',20),(292,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:23:58',20),(293,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:24:03',20),(294,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:05',20),(295,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:10',20),(296,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:13',20),(297,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:17',20),(298,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:23',20),(299,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:27',20),(300,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:32',20),(301,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:27:40',20),(302,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:18',20),(303,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:23',20),(304,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:27',20),(305,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:39',20),(306,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:46',20),(307,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:29:55',20),(308,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:30:06',20),(309,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:31:07',20),(310,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-07 16:31:13',20),(311,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:08',20),(312,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:13',20),(313,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:16',20),(314,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:20',20),(315,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:25',20),(316,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:31',20),(317,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:37',20),(318,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:54:43',20),(319,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:18',20),(320,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:23',20),(321,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:27',20),(322,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:31',20),(323,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:38',20),(324,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:44',20),(325,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:51',20),(326,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 13:58:57',20),(327,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','2017-03-08 14:00:44',20);
/*!40000 ALTER TABLE `qp_ussd_activity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_ussd_menu`
--

DROP TABLE IF EXISTS `qp_ussd_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_ussd_menu` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `language` int(11) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_ussd_menu`
--

LOCK TABLES `qp_ussd_menu` WRITE;
/*!40000 ALTER TABLE `qp_ussd_menu` DISABLE KEYS */;
INSERT INTO `qp_ussd_menu` VALUES (1,0,'Registration Menu',1,'Welcome to Qopah. Select Language'),(2,0,'Home Menu',1,'Welcome to Qopah'),(3,0,'Qopah Menu',1,'Please Select The Amount to Qopah'),(4,0,'Edit My Profile',1,'Please select the information to edit or change'),(5,0,'Update Language',1,'Reply with the language option:'),(6,0,'Change Meter Number',1,'The meter number received is incorrect. Please reply with option selected:'),(7,1,'1:English',1,''),(8,1,'2:Portoguese',1,''),(9,1,'3:Creole',1,''),(10,2,'1:Qopah Power',1,''),(11,2,'2:Purchase Power',1,''),(12,2,'3:Edit my Profile',1,''),(13,2,'4:Change my PIN',1,''),(14,2,'5:Help',1,''),(15,2,'\r\n11:Exit',1,''),(16,4,'1:Change Meter',1,''),(17,4,'2:Remove Meter',1,''),(18,4,'3:My Name',1,''),(19,4,'4:My Email',1,''),(20,4,'5:Language',1,''),(21,4,'6:Change my ID/PIN',1,''),(22,5,'1:English',1,''),(23,5,'2:Portoguese',1,''),(24,5,'3:Creole',1,''),(25,6,'1:Retry',1,''),(26,6,'2:Ignore previous meter number',1,'');
/*!40000 ALTER TABLE `qp_ussd_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_ussd_menu_tree`
--

DROP TABLE IF EXISTS `qp_ussd_menu_tree`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_ussd_menu_tree` (
  `menu_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `language` int(11) NOT NULL,
  `message` text NOT NULL,
  `level` int(11) NOT NULL,
  `param` int(11) NOT NULL,
  `previous_param` int(11) NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_ussd_menu_tree`
--

LOCK TABLES `qp_ussd_menu_tree` WRITE;
/*!40000 ALTER TABLE `qp_ussd_menu_tree` DISABLE KEYS */;
INSERT INTO `qp_ussd_menu_tree` VALUES (14,0,'Qopah Products',1,'Welcome to Qopah {{userName}} \\n Select a product \\n 1. Qopah Power\\n 2. Qopah Cash \\n 3. Qopah Airtime \\n 4. My Account \\n 5. Merchants \\n 6. SMEs',0,0,0),(15,0,'Qopah Stima',1,'Qopah Stima \\n 1. Qopah Units \\n 2. Edit Meter \\n 3. Check Balance \\n 4. Buy Units',1,1,0),(16,0,'Qopah Stima',1,'Select Number of units to borrow \\n 1. 10 Units \\n 2. 20 Units \\n 3. 30 units \\n 4. 40 units',2,1,1),(17,0,'Qopah Stima',1,'Confirm borrow 10 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,1,1),(18,0,'Qopah Stima',1,'Qopah Stima request has been received and is being processed. You will receive a confirmation SMS shortly.',4,100,1),(19,0,'Qopah Stima',1,'Confirm Edit Meter Number to {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,99,2),(20,0,'Qopah Stima',1,'Qopah Stima request has been received and is being processed. You will receive a confirmation SMS shortly.',4,100,2),(21,0,'Qopah Stima',1,'Qopah Stima Cancelled',4,909,1),(22,0,'Qopah Stima',1,'Qopah Stima Cancelled',4,909,2),(23,0,'Qopah Stima',1,'Edit Meter Number {{meterNumber}}. Enter new meter number',2,2,1),(24,0,'Qopah Stima',1,'Meter Number {{meterNumber}} updated successfully. You will receive a confirmation SMS shortly.',4,99,99),(25,0,'Qopah Stima',1,'Confirm borrow 20 Units for Meter No \\n 1. Accept \\n 2. Decline',3,2,1),(26,0,'Qopah Stima',1,'Incorrect PIN supplied. Edit Meter Cancelled',4,909,909),(27,1,'Qopah Products',1,'Welcome to Qopah Cash \\n 1. Qopah Cash \\n 2. Check Balance \\n 3. Pay loan',1,2,0),(28,1,'Qopah Cash',1,'Qopah Cash. Select amount to borrow. \\n 1. 500 \\n 2. 1000 \\n 3. 2000 \\n 4. 5000',2,1,2),(30,0,'Qopah Stima',1,'Confirm borrow 30 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,3,1),(31,0,'Qopah Stima',1,'Confirm borrow 40 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,4,1),(32,0,'Qopah Stima',1,'Qopah Stima request has been received and is being processed. You will receive a confirmation SMS shortly.',4,100,3),(33,0,'Qopah Stima',1,'Qopah Stima request has been received and is being processed. You will receive a confirmation SMS shortly.',4,100,4),(34,0,'Qopah Stima',1,'Qopah Stima Cancelled',4,909,3),(35,0,'Qopah Stima',1,'Qopah Stima Cancelled',4,909,4),(36,3,'Unsubscribe',1,'Confirm unsubscribe from Qopah. \\n 1. Accept \\n 2. Decline',2,1,4),(37,3,'unsubscribe',1,'Unsubscribe Cancelled',3,909,1),(38,1,'Qopah Cash',1,'Confirm borrow 500 units. \\n 1. Accept \\n 2. Decline',3,1,1),(39,1,'Qopah Cash',1,'Confirm borrow 1000 units. \\n 1. Accept \\n 2. Decline',3,2,1),(40,1,'Qopah Cash',1,'Confirm borrow 2000 units. \\n 1. Accept \\n 2. Decline',3,3,1),(41,1,'Qopah Cash',1,'Confirm borrow 3000 units. \\n 1. Accept \\n 2. Decline',3,4,1),(42,1,'Qopah Cash',1,'Confirm check Qash balance by entering your PIN',2,2,2),(43,2,'Qopah Airtime',1,'Welcome to Qopah Airtime \\n 1. Qopah Airtime \\n 2. Check Airtime Balance \\n 3. Buy Airtime.',1,3,0),(44,2,'Qopah Airtime',1,'Qopah Airtime. Select amount  of Airtime to borrow. \\n 1. 10 \\n 2. 100 \\n 3. 250 \\n 4. 500',2,1,3),(45,2,'Qopah Airtime',1,'Qopah Airtime. Confirm borrow Airtime KES 10. \\n 1. Accept \\n 2. Decline',3,1,1),(46,2,'Qopah Airtime',1,'Confirm check Airtime balance by entering your PIN',2,2,3),(47,2,'Qopah Airtime',1,'Qopah Airtime. Confirm borrow Airtime 100 units. \\n 1. Accept \\n 2. Decline',3,2,1),(48,2,'Qopah Airtime',1,'Qopah Airtime. Confirm borrow Airtime 250 units. \\n 1. Accept \\n 2. Decline',3,3,1),(49,2,'Qopah Airtime',1,'Qopah Airtime. Confirm borrow Airtime 500 units.\\n 1. Accept \\n 2. Decline',3,4,1),(50,3,'My Account',1,'My Account Settings \\n 1. Unsubscribe from Qopah \\n 2. Edit Registration Details \\n 3. Change PIN \\n 4. Check Balances.',1,4,0),(51,3,'My Account',1,'My Account. Select item to Edit in Profile \\n 1. Names: {{fullName}} \\n 2. ID Number: {{idNumber}}',2,2,4),(52,3,'edit profile',1,'My Account. Edit names. \\n Reply with new full names',3,1,2),(53,3,'edit profile',1,'My Account. Edit ID Number. \\n Reply with new ID Number',3,2,2),(54,3,'profile',1,'My Account. Change PIN. \\n Reply with new PIN',2,3,4),(55,3,'My Account',1,'My Account. Check Balance. \\n Reply with PIN.',2,4,4),(56,99,'Qopah Init',1,'Welcome to Qopah {{userName}} \\n Enter your pin.',99,99,99),(57,4,'Merchants',1,'Merchants \\n 1.Kopa Float ',1,5,0),(58,4,'Merchants',1,'Kopa Float \\n 1. Kopa Float. \\n 2. Lipa Float. \\n 3. Nunua Float. \\n 4. Check Balance.',2,1,5),(59,4,'Merchants',1,'Confirm Borrow Float 5,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,1,1),(60,4,'Merchants',1,'Confirm Borrow Float 10,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,2,1),(61,4,'Merchants',1,'Confirm Borrow Float 15,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,3,1),(62,4,'Merchants',1,'Confirm Borrow Float 20,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,4,1),(63,4,'Merchants',1,'Confirm Borrow Float 25,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,5,1),(64,4,'Merchants',1,'Kopa Float \\n 1. 5,000 Kshs. \\n 2. 10,000 Kshs. \\n 3. 15,000 Kshs. \\n 4. 20,000 Kshs. \\n 5. 25,000 Kshs.',3,1,1),(65,4,'Merchants',1,'Nunua Float \\n 1. 5,000 Kshs. \\n 2. 10,000 Kshs. \\n 3. 15,000 Kshs. \\n 4. 20,000 Kshs. \\n 5. 25,000 Kshs.',3,3,1),(66,4,'Merchants',1,'Confirm Buy Float 5,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,1,3),(67,4,'Merchants',1,'Confirm Buy Float 10,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,2,3),(68,4,'Merchants',1,'Confirm Buy Float 15,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,3,3),(69,4,'Merchants',1,'Confirm Buy Float 20,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,4,3),(70,4,'Merchants',1,'Confirm Buy Float 25,000 Kshs. \\n 1. Accept. \\n 2. Decline. ',4,5,3),(73,4,'Merchants',1,'Lipa Float \\n Reply with Amount.',3,2,1),(74,0,'Qopah Stima',1,'Select Number of units to buy \\n 1. 10 Units \\n 2. 20 Units \\n 3. 30 units \\n 4. 40 units',2,4,1),(75,0,'Qopah Stima',1,'Confirm buy 10 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,1,4),(76,0,'Qopah Stima',1,'Confirm buy 20 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,2,4),(77,0,'Qopah Stima',1,'Confirm buy 30 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,3,4),(78,0,'Qopah Stima',1,'Confirm buy 40 Units for Meter No {{meterNumber}} \\n 1. Accept \\n 2. Decline',3,4,4),(82,2,'Qopah Airtime',1,'Qopah Airtime. Select amount  of Airtime to buy. \\n 1. 50 \\n 2. 100 \\n 3. 250 \\n 4. 500',2,3,3),(83,2,'Qopah Airtime',1,'Qopah Airtime. Confirm buy Airtime KES 10. \\n 1. Accept \\n 2. Decline',3,1,3),(84,2,'Qopah Airtime',1,'Qopah Airtime. Confirm buy Airtime 100 units. \\n 1. Accept \\n 2. Decline',3,2,3),(85,2,'Qopah Airtime',1,'Qopah Airtime. Confirm buy Airtime 250 units. \\n 1. Accept \\n 2. Decline',3,3,3),(86,2,'Qopah Airtime',1,'Qopah Airtime. Confirm buy Airtime 500 units.\\n 1. Accept \\n 2. Decline',3,4,3),(87,1,'Qopah Cash',1,'Pay Cash \\n Reply with Amount.',2,3,2),(88,5,'Merchants',1,'SME \\n 1. Borrow Cash ',1,6,0);
/*!40000 ALTER TABLE `qp_ussd_menu_tree` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `qp_ussd_session`
--

DROP TABLE IF EXISTS `qp_ussd_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `qp_ussd_session` (
  `qp_ussd_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) NOT NULL,
  `session_id` varchar(50) NOT NULL,
  `narration` varchar(70) NOT NULL,
  `param` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL,
  `previousParam` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`qp_ussd_session_id`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `qp_ussd_session`
--

LOCK TABLES `qp_ussd_session` WRITE;
/*!40000 ALTER TABLE `qp_ussd_session` DISABLE KEYS */;
INSERT INTO `qp_ussd_session` VALUES (1,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','0','0','0','0','2017-02-28 08:41:37'),(2,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','0','0','0','0','2017-03-01 12:52:43'),(3,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','0','0','0','0','2017-03-01 12:53:16'),(4,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','0','0','0','0','2017-03-01 13:01:01'),(5,'254714293809','ATUid_165e8bc61a92afafarted2a1ad3c7af72eab','6','6','1','0','2017-03-01 13:01:07'),(6,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:07:45'),(7,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','1111*6','6','1','0','2017-03-02 10:07:52'),(8,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','1111*6','6','1','0','2017-03-02 10:08:50'),(9,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:08:56'),(10,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-02 10:09:02'),(11,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:11:06'),(12,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6','6','1','0','2017-03-02 10:11:11'),(13,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:11:43'),(14,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6*00*6','6','1','0','2017-03-02 10:11:48'),(15,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:13:46'),(16,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 10:19:36'),(17,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6*00*6*00*00*6','6','1','0','2017-03-02 10:20:03'),(18,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 12:28:01'),(19,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-02 12:28:08'),(20,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 12:30:18'),(21,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6','6','1','0','2017-03-02 12:30:23'),(22,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 12:33:22'),(23,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6*00*6','6','1','0','2017-03-02 12:33:27'),(24,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*00*6*00*6','6','1','0','2017-03-02 13:37:19'),(25,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-02 13:37:33'),(26,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-02 13:37:38'),(27,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 04:56:04'),(28,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 04:56:11'),(29,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 04:56:38'),(30,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1','1','2','6','2017-03-06 05:11:18'),(31,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 05:11:29'),(32,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1*00*6','6','1','0','2017-03-06 05:11:33'),(33,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1*00*6*2','2','2','6','2017-03-06 05:11:38'),(34,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 05:27:29'),(35,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1*00*6*2*00*6','6','1','0','2017-03-06 05:27:34'),(36,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1*00*6*2*00*6*1','1','2','6','2017-03-06 05:27:41'),(37,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*1*00*6*2*00*6*1','1','2','6','2017-03-06 05:28:01'),(38,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 06:51:59'),(39,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 06:52:04'),(40,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1','1','2','6','2017-03-06 06:52:08'),(41,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil','Hashi Oil','3','1','2017-03-06 06:52:18'),(42,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 06:56:15'),(43,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 06:56:20'),(44,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1','1','2','6','2017-03-06 06:56:24'),(45,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil','Hashi Oil','3','1','2017-03-06 06:56:42'),(46,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil','Hashi Oil','3','1','2017-03-06 06:57:02'),(47,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 07:10:14'),(48,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 07:10:17'),(49,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1','1','2','6','2017-03-06 07:10:21'),(50,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil','Hashi Oil','3','1','2017-03-06 07:10:31'),(51,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt5678','rt5678','4','Hashi Oil','2017-03-06 07:10:37'),(52,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt5678*Rongai','Rongai','5','rt5678','2017-03-06 07:10:44'),(53,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt5678*Rongai','Rongai','5','rt5678','2017-03-06 07:10:51'),(54,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 07:14:22'),(55,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 07:14:26'),(56,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1','1','2','6','2017-03-06 07:14:32'),(57,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil','Hashi Oil','3','1','2017-03-06 07:14:38'),(58,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt4567','rt4567','4','Hashi Oil','2017-03-06 07:14:44'),(59,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt4567*234 apollo','234 apollo','5','rt4567','2017-03-06 07:14:51'),(60,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi Oil*rt4567*234 apollo*342 nanyuki','342 nanyuki','6','234 apollo','2017-03-06 07:15:11'),(61,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 07:18:07'),(62,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6','6','1','0','2017-03-06 07:18:11'),(63,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1','1','2','6','2017-03-06 07:18:15'),(64,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi oil','Hashi oil','3','1','2017-03-06 07:18:23'),(65,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi oil*rt67','rt67','4','Hashi oil','2017-03-06 07:19:56'),(66,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','00*6*1*Hashi oil*rt67*rongai','rongai','5','rt67','2017-03-06 07:22:48'),(67,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7af72eab','0','0','0','0','2017-03-06 07:28:38'),(68,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 07:29:30'),(69,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6','6','1','0','2017-03-06 07:29:36'),(70,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1','1','2','6','2017-03-06 07:29:43'),(71,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai','Rongai','3','1','2017-03-06 07:29:52'),(72,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345','cert2345','4','Rongai','2017-03-06 07:30:03'),(73,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai','rongai','5','cert2345','2017-03-06 07:30:11'),(74,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234','1234','6','rongai','2017-03-06 07:30:18'),(75,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1','1','7','1234','2017-03-06 07:30:28'),(76,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 07:37:35'),(77,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6','6','1','0','2017-03-06 07:37:42'),(78,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1','1','2','6','2017-03-06 07:37:45'),(79,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254','Grod254','3','1','2017-03-06 07:37:56'),(80,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254*rt4567','rt4567','4','Grod254','2017-03-06 07:38:01'),(81,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1111*6*1*Rongai*cert2345*rongai*1234*1*00*6*1*Grod254*rt4567*Rongai','Rongai','5','rt4567','2017-03-06 07:38:07'),(82,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','6','Rongai','2017-03-06 07:38:50'),(83,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1','1','7','0','2017-03-06 07:38:56'),(84,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 07:39:22'),(85,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6','6','1','0','2017-03-06 07:39:26'),(86,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1','1','2','6','2017-03-06 07:39:30'),(87,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1*Grid 254','Grid 254','3','1','2017-03-06 07:39:37'),(88,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1*Grid 254*rt4567','rt4567','4','Grid 254','2017-03-06 07:39:43'),(89,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1*Grid 254*rt4567*Rongai','Rongai','5','rt4567','2017-03-06 07:39:49'),(90,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1*Grid 254*rt4567*Rongai*768','768','6','Rongai','2017-03-06 07:39:53'),(91,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','1*00*6*1*Grid 254*rt4567*Rongai*768*1','1','7','768','2017-03-06 07:40:10'),(92,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 07:40:45'),(93,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 07:40:48'),(94,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 07:40:52'),(95,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Grid254','Grid254','3','1','2017-03-06 07:40:59'),(96,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Grid254*rt5678','rt5678','4','Grid254','2017-03-06 07:41:03'),(97,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Grid254*rt5678*Rongai','Rongai','5','rt5678','2017-03-06 07:41:14'),(98,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Grid254*rt5678*Rongai*234','234','6','Rongai','2017-03-06 07:41:19'),(99,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Grid254*rt5678*Rongai*234*1','1','7','234','2017-03-06 07:41:22'),(100,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:00:38'),(101,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:00:46'),(102,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 08:00:55'),(103,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai','Rongai','3','1','2017-03-06 08:01:02'),(104,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555','RT555','4','Rongai','2017-03-06 08:01:09'),(105,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai','Rongai','5','RT555','2017-03-06 08:02:44'),(106,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556','34556','6','Rongai','2017-03-06 08:03:54'),(107,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:04:19'),(108,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6','6','1','0','2017-03-06 08:04:23'),(109,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6*1','1','2','6','2017-03-06 08:04:29'),(110,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala','Koala','3','1','2017-03-06 08:04:34'),(111,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678','rt5678','4','Koala','2017-03-06 08:04:43'),(112,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678*Rongai','Rongai','5','rt5678','2017-03-06 08:04:51'),(113,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Rongai*RT555*Rongai*34556*00*6*1*Koala*rt5678*Rongai*778','778','6','Rongai','2017-03-06 08:04:56'),(114,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:12:26'),(115,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:12:31'),(116,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 08:12:36'),(117,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala','Koala','3','1','2017-03-06 08:12:41'),(118,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*123rt56','123rt56','4','Koala','2017-03-06 08:12:48'),(119,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*123rt56*Rongai','Rongai','5','123rt56','2017-03-06 08:12:54'),(120,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*123rt56*Rongai*234','234','6','Rongai','2017-03-06 08:13:01'),(121,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*123rt56*Rongai*234*1','1','7','234','2017-03-06 08:13:15'),(122,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:16:21'),(123,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:16:25'),(124,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 08:16:30'),(125,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala','Koala','3','1','2017-03-06 08:16:36'),(126,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt56789','rt56789','4','Koala','2017-03-06 08:16:42'),(127,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt56789*Rongai','Rongai','5','rt56789','2017-03-06 08:16:51'),(128,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt56789*Rongai*7865','7865','6','Rongai','2017-03-06 08:16:56'),(129,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt56789*Rongai*7865*1','1','7','7865','2017-03-06 08:17:04'),(130,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:33:58'),(131,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:34:02'),(132,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 08:34:06'),(133,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala','Koala','3','1','2017-03-06 08:34:13'),(134,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt5463','rt5463','4','Koala','2017-03-06 08:34:21'),(135,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt5463*Rongai','Rongai','5','rt5463','2017-03-06 08:34:26'),(136,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt5463*Rongai*546','546','6','Rongai','2017-03-06 08:34:31'),(137,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Koala*rt5463*Rongai*546*1','1','7','546','2017-03-06 08:34:46'),(138,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:35:11'),(139,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:35:15'),(140,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 08:35:22'),(141,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Qopah Fintech','Qopah Fintech','3','1','2017-03-06 08:35:33'),(142,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Qopah Fintech*rt5678','rt5678','4','Qopah Fintech','2017-03-06 08:35:40'),(143,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Qopah Fintech*rt5678*Karen','Karen','5','rt5678','2017-03-06 08:35:46'),(144,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Qopah Fintech*rt5678*Karen*123','123','6','Karen','2017-03-06 08:35:51'),(145,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6*1*Qopah Fintech*rt5678*Karen*123*1','1','7','123','2017-03-06 08:37:04'),(146,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:41:20'),(147,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:41:25'),(148,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:53:15'),(149,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:53:19'),(150,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 08:56:48'),(151,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 08:56:53'),(152,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:00:33'),(153,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:00:37'),(154,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:01:18'),(155,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:01:23'),(156,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:01:28'),(157,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:03:44'),(158,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:03:48'),(159,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:07:35'),(160,'254714293809','ATUid_165e8bc61a92afafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:07:39'),(161,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:22:01'),(162,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6','6','1','0','2017-03-06 09:22:06'),(163,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1','1','2','6','2017-03-06 09:22:11'),(164,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1*Qopah','Qopah','3','1','2017-03-06 09:22:18'),(165,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1*Qopah*rt5463','rt5463','4','Qopah','2017-03-06 09:22:23'),(166,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1*Qopah*rt5463*Rongai','Rongai','5','rt5463','2017-03-06 09:22:30'),(167,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1*Qopah*rt5463*Rongai*5678','5678','6','Rongai','2017-03-06 09:22:34'),(168,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','1111*6*1*Qopah*rt5463*Rongai*5678*1','1','7','5678','2017-03-06 09:22:40'),(169,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:24:41'),(170,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:24:46'),(171,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 09:24:49'),(172,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah','Qopah','3','1','2017-03-06 09:24:55'),(173,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5678','rt5678','4','Qopah','2017-03-06 09:25:00'),(174,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5678*Rongai','Rongai','5','rt5678','2017-03-06 09:25:06'),(175,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5678*Rongai*768','768','6','Rongai','2017-03-06 09:25:10'),(176,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5678*Rongai*768*1','1','7','768','2017-03-06 09:25:15'),(177,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5678*Rongai*768*1','1','7','768','2017-03-06 09:27:12'),(178,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:33:59'),(179,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:34:04'),(180,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 09:34:07'),(181,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah','Qopah','3','1','2017-03-06 09:34:15'),(182,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt67543','rt67543','4','Qopah','2017-03-06 09:34:21'),(183,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt67543*KAren','KAren','5','rt67543','2017-03-06 09:34:27'),(184,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt67543*KAren*2345','2345','6','KAren','2017-03-06 09:34:33'),(185,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt67543*KAren*2345*1','1','7','2345','2017-03-06 09:34:41'),(186,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:36:12'),(187,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:36:15'),(188,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 09:36:22'),(189,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah','Qopah','3','1','2017-03-06 09:36:49'),(190,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt56789','rt56789','4','Qopah','2017-03-06 09:36:54'),(191,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt56789*Rongai','Rongai','5','rt56789','2017-03-06 09:37:09'),(192,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt56789*Rongai*342','342','6','Rongai','2017-03-06 09:37:14'),(193,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt56789*Rongai*342*1','1','7','342','2017-03-06 09:37:33'),(194,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:40:24'),(195,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:40:29'),(196,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 09:40:36'),(197,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah','Qopah','3','1','2017-03-06 09:40:49'),(198,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5453','rt5453','4','Qopah','2017-03-06 09:40:55'),(199,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5453*Rongai','Rongai','5','rt5453','2017-03-06 09:41:02'),(200,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5453*Rongai*234','234','6','Rongai','2017-03-06 09:41:07'),(201,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah*rt5453*Rongai*234*1','1','7','234','2017-03-06 09:41:14'),(202,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-06 09:45:01'),(203,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-06 09:45:04'),(204,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-06 09:45:20'),(205,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah Services','Qopah Services','3','1','2017-03-06 09:45:30'),(206,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah Services*rt5463','rt5463','4','Qopah Services','2017-03-06 09:45:37'),(207,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah Services*rt5463*Karen','Karen','5','rt5463','2017-03-06 09:45:42'),(208,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah Services*rt5463*Karen*342','342','6','Karen','2017-03-06 09:45:47'),(209,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*Qopah Services*rt5463*Karen*342*1','1','7','342','2017-03-06 09:46:00'),(210,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 06:07:50'),(211,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 06:07:57'),(212,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 06:09:21'),(213,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*00*6','6','1','0','2017-03-07 06:09:25'),(214,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*00*6*1','1','2','6','2017-03-07 06:09:31'),(215,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 06:11:32'),(216,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*00*6*1*00*6','6','1','0','2017-03-07 06:11:39'),(217,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*00*6*1*00*6','6','1','0','2017-03-07 06:11:43'),(218,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 06:18:25'),(219,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 06:18:30'),(220,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 06:18:34'),(221,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 06:37:57'),(222,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 06:44:41'),(223,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 06:44:46'),(224,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 06:44:52'),(225,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 06:45:00'),(226,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 07:19:56'),(227,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 07:20:01'),(228,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 07:20:05'),(229,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*100000','100000','3','1','2017-03-07 07:20:18'),(230,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*100000*4','4','4','100000','2017-03-07 07:20:25'),(231,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*100000*4*1','1','5','4','2017-03-07 07:20:35'),(232,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*100000*4*1*1','1','6','1','2017-03-07 07:20:56'),(233,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 07:41:51'),(234,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 07:41:57'),(235,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 07:42:03'),(236,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*3000000','3000000','3','1','2017-03-07 07:42:10'),(237,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*3000000*1','1','4','3000000','2017-03-07 07:42:18'),(238,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*3000000*1*2','2','5','1','2017-03-07 07:42:53'),(239,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*3000000*1*2*1','1','6','2','2017-03-07 07:42:58'),(240,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 07:59:11'),(241,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 07:59:45'),(242,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 07:59:49'),(243,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','KES','2','6','2017-03-07 07:59:52'),(244,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','KES','2017-03-07 07:59:56'),(245,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000','20000','4','1','2017-03-07 08:01:00'),(246,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*1','cheque','5','20000','2017-03-07 08:03:29'),(247,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*1*1','1','6','cheque','2017-03-07 08:05:16'),(248,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 08:13:59'),(249,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 08:14:04'),(250,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 08:14:11'),(251,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','1','2017-03-07 08:14:16'),(252,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*3456666','3456666','4','1','2017-03-07 08:14:23'),(253,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*3456666*2','2','5','3456666','2017-03-07 08:14:27'),(254,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*3456666*2*1','1','6','2','2017-03-07 08:14:35'),(255,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 12:00:05'),(256,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 12:00:21'),(257,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 12:00:26'),(258,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','1','2017-03-07 12:00:33'),(259,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000','20000','4','1','2017-03-07 12:00:43'),(260,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*4','4','5','20000','2017-03-07 12:00:52'),(261,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*4*1','1','6','4','2017-03-07 12:01:00'),(262,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 13:13:05'),(263,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 13:13:08'),(264,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 13:13:12'),(265,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','1','2017-03-07 13:13:17'),(266,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000','20000','4','1','2017-03-07 13:13:22'),(267,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*1','1','5','20000','2017-03-07 13:13:27'),(268,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*20000*1*1','1','6','1','2017-03-07 13:13:42'),(269,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 13:23:24'),(270,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 13:23:27'),(271,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 13:23:31'),(272,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2','2','3','1','2017-03-07 13:23:40'),(273,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000','900000','4','2','2017-03-07 13:23:49'),(274,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5','5','5','900000','2017-03-07 13:23:53'),(275,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2','2','6','5','2017-03-07 13:23:58'),(276,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1','1','7','2','2017-03-07 13:24:03'),(277,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 13:27:05'),(278,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6','6','1','0','2017-03-07 13:27:10'),(279,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1','1','2','6','2017-03-07 13:27:13'),(280,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1*2','2','3','1','2017-03-07 13:27:17'),(281,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1*2*90000','90000','4','2','2017-03-07 13:27:23'),(282,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1','1','5','90000','2017-03-07 13:27:27'),(283,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1*2','2','6','1','2017-03-07 13:27:32'),(284,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*900000*5*2*1*00*6*1*2*90000*1*2*1','1','7','2','2017-03-07 13:27:40'),(285,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-07 13:29:18'),(286,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-07 13:29:23'),(287,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-07 13:29:27'),(288,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2','2','3','1','2017-03-07 13:29:39'),(289,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*4000000','4000000','4','2','2017-03-07 13:29:45'),(290,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*4000000*4','4','5','4000000','2017-03-07 13:29:55'),(291,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*4000000*4*2','2','6','4','2017-03-07 13:30:06'),(292,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*4000000*4*2*1','1','7','2','2017-03-07 13:31:07'),(293,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*2*4000000*4*2*1','1','7','2','2017-03-07 13:31:13'),(294,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-08 10:54:08'),(295,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-08 10:54:13'),(296,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-08 10:54:16'),(297,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','1','2017-03-08 10:54:20'),(298,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*900000','900000','4','1','2017-03-08 10:54:25'),(299,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*900000*3','3','5','900000','2017-03-08 10:54:31'),(300,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*900000*3*1','1','6','3','2017-03-08 10:54:37'),(301,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*900000*3*1*1','1','7','1','2017-03-08 10:54:43'),(302,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','0','0','0','0','2017-03-08 10:58:18'),(303,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6','6','1','0','2017-03-08 10:58:23'),(304,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1','1','2','6','2017-03-08 10:58:27'),(305,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1','1','3','1','2017-03-08 10:58:31'),(306,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*90000','90000','4','1','2017-03-08 10:58:38'),(307,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*90000*4','4','5','90000','2017-03-08 10:58:44'),(308,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*90000*4*2','2','6','4','2017-03-08 10:58:51'),(309,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*90000*4*2*1','1','7','2','2017-03-08 10:58:57'),(310,'254714293809','ATUid_165e8bc61a92dafafarted2a1fad3c7faf72eab','00*6*1*1*90000*4*2*1','1','7','2','2017-03-08 11:00:44');
/*!40000 ALTER TABLE `qp_ussd_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-03-10 16:48:21
